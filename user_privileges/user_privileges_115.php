<?php


//This is the access privilege file
$is_admin=false;

$current_user_roles='H51';

$current_user_parent_role_seq='H1::H2::H41::H51';

$current_user_profiles=array(13,);

$profileGlobalPermission=array('1'=>1,'2'=>1,);

$profileTabsPermission=array('1'=>1,'4'=>1,'6'=>1,'7'=>0,'8'=>1,'9'=>1,'10'=>1,'13'=>1,'16'=>1,'25'=>1,'26'=>1,'27'=>1,'37'=>1,'38'=>1,'41'=>1,'42'=>0,'44'=>1,'45'=>1,'49'=>1,'50'=>0,'51'=>0,'52'=>1,'53'=>1,'54'=>1,'55'=>1,'56'=>1,'57'=>1,'58'=>0,'59'=>0,'28'=>1,'3'=>0,);

$profileActionPermission=array(7=>array(0=>0,1=>0,2=>1,4=>0,10=>0,),25=>array(0=>1,1=>1,2=>1,4=>1,6=>1,13=>1,),42=>array(0=>1,1=>1,2=>1,4=>0,),51=>array(0=>1,1=>1,2=>1,4=>0,14=>0,),52=>array(0=>1,1=>1,2=>1,4=>0,),53=>array(0=>1,1=>1,2=>1,4=>0,),59=>array(0=>0,1=>0,2=>0,3=>0,4=>0,),);

$current_user_groups=array();

$subordinate_roles=array('H50','H42','H43','H44','H45','H46','H47','H48','H49',);

$parent_roles=array('H1','H2','H41',);

$subordinate_roles_users=array('H50'=>array(114,116,),'H42'=>array(92,),'H43'=>array(93,94,95,96,119,130,),'H44'=>array(97,),'H45'=>array(98,99,103,104,105,112,117,118,120,123,127,128,),'H46'=>array(100,),'H47'=>array(101,102,106,107,108,109,121,),'H48'=>array(110,),'H49'=>array(111,),);

$user_info=array('user_name'=>'alain.roca','is_admin'=>'off','user_password'=>'$1$al000000$QPltoDoGzg1NozYIw9zDt1','confirm_password'=>'$1$al000000$QPltoDoGzg1NozYIw9zDt1','first_name'=>'TAM Alain','last_name'=>'Roca','roleid'=>'H51','email1'=>'alainroca.pro@gmail.com','status'=>'Active','activity_view'=>'Today','lead_view'=>'Today','hour_format'=>'24','end_hour'=>'','start_hour'=>'00:00','title'=>'','phone_work'=>'','department'=>'','phone_mobile'=>'','reports_to_id'=>'','phone_other'=>'','email2'=>'','phone_fax'=>'','secondaryemail'=>'notify-tam@ibrokers.trade','phone_home'=>'','date_format'=>'dd-mm-yyyy','signature'=>'','description'=>'','address_street'=>'','address_city'=>'','address_state'=>'','address_postalcode'=>'','address_country'=>'','accesskey'=>'05EKyprgmOKRituQ','time_zone'=>'Europe/Amsterdam','currency_id'=>'2','currency_grouping_pattern'=>'123,456,789','currency_decimal_separator'=>'.','currency_grouping_separator'=>'&nbsp;','currency_symbol_placement'=>'$1.0','imagename'=>'','internal_mailer'=>'0','theme'=>'nature','language'=>'en_us','reminder_interval'=>'15 Minutes','phone_crm_extension'=>'','no_of_currency_decimals'=>'2','truncate_trailing_zeros'=>'0','dayoftheweek'=>'Monday','callduration'=>'5','othereventduration'=>'5','calendarsharedtype'=>'public','default_record_view'=>'Summary','leftpanelhide'=>'1','rowheight'=>'narrow','defaulteventstatus'=>'Select an Option','defaultactivitytype'=>'Select an Option','hidecompletedevents'=>'0','is_owner'=>'0','smat'=>'','wdata'=>'','currency_name'=>'Euro','currency_code'=>'EUR','currency_symbol'=>'&#8364;','conv_rate'=>'1.10000','record_id'=>'','record_module'=>'','id'=>'115');
?>