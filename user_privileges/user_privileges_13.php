<?php


//This is the access privilege file
$is_admin=false;

$current_user_roles='H54';

$current_user_parent_role_seq='H1::H2::H54';

$current_user_profiles=array(10,);

$profileGlobalPermission=array('1'=>1,'2'=>1,);

$profileTabsPermission=array('1'=>0,'4'=>1,'6'=>1,'7'=>0,'8'=>0,'9'=>1,'10'=>0,'13'=>1,'16'=>1,'25'=>0,'26'=>0,'27'=>1,'33'=>1,'37'=>0,'38'=>1,'41'=>1,'42'=>0,'44'=>0,'45'=>1,'49'=>1,'50'=>0,'51'=>0,'52'=>0,'53'=>0,'54'=>1,'55'=>0,'56'=>0,'57'=>0,'58'=>0,'59'=>0,'28'=>0,'3'=>0,);

$profileActionPermission=array(4=>array(0=>1,1=>1,2=>1,4=>1,10=>0,),6=>array(0=>1,1=>1,2=>1,4=>1,10=>0,),7=>array(0=>1,1=>1,2=>1,4=>0,9=>1,10=>0,),8=>array(0=>1,1=>1,2=>1,4=>0,),9=>array(0=>1,1=>1,2=>1,4=>1,5=>0,6=>0,),13=>array(0=>1,1=>1,2=>1,4=>1,10=>0,),16=>array(0=>1,1=>1,2=>1,4=>1,5=>0,6=>0,),25=>array(0=>1,1=>1,2=>1,4=>1,6=>0,13=>0,),26=>array(0=>1,1=>1,2=>1,4=>0,),33=>array(0=>1,1=>1,2=>1,4=>1,11=>1,12=>1,),38=>array(0=>1,1=>1,2=>1,4=>0,),42=>array(0=>1,1=>1,2=>1,4=>0,),51=>array(0=>1,1=>1,2=>1,4=>0,14=>0,),52=>array(0=>1,1=>1,2=>1,4=>0,),53=>array(0=>1,1=>1,2=>1,4=>0,),55=>array(0=>1,1=>1,2=>1,3=>0,4=>0,),56=>array(0=>1,1=>1,2=>1,3=>0,4=>0,),59=>array(0=>0,1=>0,2=>0,3=>0,4=>0,),);

$current_user_groups=array(30,28,);

$subordinate_roles=array('H55','H56',);

$parent_roles=array('H1','H2',);

$subordinate_roles_users=array('H55'=>array(140,),'H56'=>array(141,),);

$user_info=array('user_name'=>'test','is_admin'=>'off','user_password'=>'$1$te000000$NxMCb7p8Ey2zEPIQohrFO/','confirm_password'=>'$1$te000000$NxMCb7p8Ey2zEPIQohrFO/','first_name'=>'NEW','last_name'=>'TEST','roleid'=>'H54','email1'=>'test@arabic-forex.com','status'=>'Active','activity_view'=>'Today','lead_view'=>'Today','hour_format'=>'24','end_hour'=>'','start_hour'=>'00:00','title'=>'','phone_work'=>'','department'=>'','phone_mobile'=>'','reports_to_id'=>'','phone_other'=>'','email2'=>'','phone_fax'=>'','secondaryemail'=>'','phone_home'=>'','date_format'=>'dd-mm-yyyy','signature'=>'Kind regards,&lt;br /&gt;&lt;p&gt;		&lt;b&gt;andrew test&lt;/b&gt;&lt;br /&gt;		job2 	&lt;/p&gt;&lt;p&gt;Office: &lt;a style=&quot;color:#41a7fc;text-decoration:none;&quot;&gt;+44-20-3670-1520&lt;/a&gt;&lt;br /&gt;Skype: &lt;a style=&quot;color:#41a7fc;text-decoration:none;&quot;&gt;xxx&lt;/a&gt;&lt;br /&gt;Web: &lt;a href=&quot;http://www.tradefintech.com&quot; style=&quot;color:#41a7fc;text-decoration:none;&quot;&gt;www.tradefintech.com&lt;/a&gt;&lt;br /&gt;&lt;/p&gt;	&lt;p&gt;		&lt;a href=&quot;http://www.tradefintech.com&quot;&gt;&lt;img src=&quot;https://www.tradefintech.com/misc/signatures/images/logo.jpg&quot; alt=&quot;tradefintech&quot; title=&quot;tradefintech&quot; /&gt;&lt;/a&gt;	&lt;/p&gt;','description'=>'','address_street'=>'','address_city'=>'','address_state'=>'','address_postalcode'=>'','address_country'=>'','accesskey'=>'ka6eov71afxUc0G7','time_zone'=>'Pacific/Midway','currency_id'=>'1','currency_grouping_pattern'=>'123,456,789','currency_decimal_separator'=>'.','currency_grouping_separator'=>',','currency_symbol_placement'=>'$1.0','imagename'=>'','internal_mailer'=>'1','theme'=>'firebrick','language'=>'en_us','reminder_interval'=>'1 Minute','phone_crm_extension'=>'','no_of_currency_decimals'=>'2','truncate_trailing_zeros'=>'0','dayoftheweek'=>'Monday','callduration'=>'5','othereventduration'=>'5','calendarsharedtype'=>'public','default_record_view'=>'Summary','leftpanelhide'=>'0','rowheight'=>'narrow','defaulteventstatus'=>'Select an Option','defaultactivitytype'=>'Select an Option','hidecompletedevents'=>'0','is_owner'=>'0','smat'=>'s6QLFHTjpIx5GdAeBCRypw','wdata'=>'{&quot;113&quot;:{&quot;oid&quot;:&quot;13&quot;,&quot;dt&quot;:&quot;&quot;},&quot;120&quot;:{&quot;oid&quot;:&quot;&quot;,&quot;dt&quot;:{&quot;start&quot;:&quot;2017-07-01&quot;,&quot;end&quot;:&quot;2017-09-30&quot;}}}','currency_name'=>'USA, Dollars','currency_code'=>'USD','currency_symbol'=>'&#36;','conv_rate'=>'1.00000','record_id'=>'','record_module'=>'','id'=>'13');
?>