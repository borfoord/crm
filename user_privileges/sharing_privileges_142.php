<?php


//This is the sharing access privilege file
$defaultOrgSharingPermission=array('2'=>2,'4'=>3,'6'=>3,'7'=>3,'9'=>3,'13'=>3,'16'=>3,'20'=>2,'21'=>2,'22'=>2,'23'=>2,'26'=>0,'8'=>3,'14'=>2,'30'=>2,'33'=>3,'36'=>2,'38'=>3,'40'=>2,'42'=>3,'46'=>2,'47'=>2,'48'=>2,'18'=>2,'10'=>3,'51'=>3,'52'=>3,'53'=>3,'55'=>3,'56'=>3,'57'=>2,'3'=>2,);

$related_module_share=array(2=>array(6,),13=>array(6,),20=>array(6,2,),22=>array(6,2,20,),23=>array(6,22,),);

$Leads_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$Leads_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$Leads_Emails_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$Leads_Emails_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$Accounts_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$Accounts_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$Contacts_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$Contacts_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$Accounts_Potentials_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$Accounts_Potentials_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$Accounts_HelpDesk_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$Accounts_HelpDesk_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$Accounts_Emails_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$Accounts_Emails_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$Accounts_Quotes_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$Accounts_Quotes_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$Accounts_SalesOrder_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$Accounts_SalesOrder_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$Accounts_Invoice_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$Accounts_Invoice_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$Potentials_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$Potentials_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$Potentials_Quotes_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$Potentials_Quotes_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$Potentials_SalesOrder_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$Potentials_SalesOrder_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$HelpDesk_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$HelpDesk_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$Emails_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$Emails_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$Campaigns_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$Campaigns_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$Quotes_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$Quotes_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$Quotes_SalesOrder_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$Quotes_SalesOrder_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$PurchaseOrder_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$PurchaseOrder_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$SalesOrder_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$SalesOrder_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$SalesOrder_Invoice_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$SalesOrder_Invoice_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$Invoice_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$Invoice_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$Documents_share_read_permission=array('ROLE'=>array(),'GROUP'=>array(70=>array(0=>62,1=>64,2=>66,3=>69,4=>73,5=>63,6=>65,7=>68,8=>72,9=>74,10=>76,11=>87,12=>88,13=>131,14=>132,15=>133,16=>134,17=>135,18=>136,19=>138,20=>139,21=>142,22=>143,),));

$Documents_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$SMSNotifier_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$SMSNotifier_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$ModComments_share_read_permission=array('ROLE'=>array('H27'=>array(62,64,66,69,73,),'H28'=>array(63,65,68,72,74,76,87,88,131,132,133,134,135,136,138,139,142,143,),),'GROUP'=>array());

$ModComments_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$DragNDrop_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$DragNDrop_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$MT4Accounts_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$MT4Accounts_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$Deposits_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$Deposits_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$IBService_share_read_permission=array('ROLE'=>array('H2'=>array(5,6,7,124,129,),'H16'=>array(12,),'H19'=>array(34,),'H23'=>array(59,),'H24'=>array(56,57,71,),'H25'=>array(58,),'H26'=>array(60,),'H29'=>array(77,),'H30'=>array(),'H31'=>array(),'H3'=>array(11,14,53,),'H32'=>array(),'H33'=>array(80,),'H34'=>array(81,89,),'H35'=>array(79,82,113,),'H36'=>array(78,83,),'H37'=>array(84,),'H38'=>array(),'H39'=>array(85,86,),'H10'=>array(10,),'H11'=>array(20,21,35,45,47,49,51,54,),'H12'=>array(),'H13'=>array(),'H14'=>array(),'H15'=>array(),'H17'=>array(31,),'H18'=>array(32,),'H20'=>array(37,38,),'H21'=>array(),'H22'=>array(41,44,),'H4'=>array(9,),'H5'=>array(8,33,43,),'H6'=>array(),'H7'=>array(15,24,),'H8'=>array(),'H9'=>array(),'H40'=>array(61,67,),'H53'=>array(137,),'H27'=>array(62,64,66,69,73,),'H28'=>array(63,65,68,72,74,76,87,88,131,132,133,134,135,136,138,139,142,143,),'H41'=>array(90,91,),'H51'=>array(115,),'H50'=>array(114,116,),'H42'=>array(92,),'H43'=>array(93,94,95,96,119,130,),'H44'=>array(97,),'H45'=>array(98,99,103,104,105,112,117,118,120,123,127,128,),'H46'=>array(100,),'H47'=>array(101,102,106,107,108,109,121,),'H48'=>array(110,),'H49'=>array(111,),'H52'=>array(122,),'H54'=>array(13,),'H55'=>array(140,),'H56'=>array(141,),'H57'=>array(),'H58'=>array(),'H59'=>array(),),'GROUP'=>array());

$IBService_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

$SProcess_share_read_permission=array('ROLE'=>array(),'GROUP'=>array());

$SProcess_share_write_permission=array('ROLE'=>array(),'GROUP'=>array());

?>