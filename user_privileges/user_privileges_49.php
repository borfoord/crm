<?php


//This is the access privilege file
$is_admin=false;

$current_user_roles='H11';

$current_user_parent_role_seq='H1::H2::H3::H10::H11';

$current_user_profiles=array(10,);

$profileGlobalPermission=array('1'=>1,'2'=>1,);

$profileTabsPermission=array('1'=>0,'4'=>1,'6'=>1,'7'=>0,'8'=>0,'9'=>1,'10'=>0,'13'=>1,'16'=>1,'25'=>0,'26'=>0,'27'=>1,'33'=>1,'37'=>0,'38'=>1,'41'=>1,'42'=>0,'44'=>0,'45'=>1,'49'=>1,'50'=>0,'51'=>0,'52'=>0,'53'=>0,'54'=>1,'55'=>0,'56'=>0,'57'=>0,'58'=>0,'59'=>0,'28'=>0,'3'=>0,);

$profileActionPermission=array(4=>array(0=>1,1=>1,2=>1,4=>1,10=>0,),6=>array(0=>1,1=>1,2=>1,4=>1,10=>0,),7=>array(0=>1,1=>1,2=>1,4=>0,9=>1,10=>0,),8=>array(0=>1,1=>1,2=>1,4=>0,),9=>array(0=>1,1=>1,2=>1,4=>1,5=>0,6=>0,),13=>array(0=>1,1=>1,2=>1,4=>1,10=>0,),16=>array(0=>1,1=>1,2=>1,4=>1,5=>0,6=>0,),25=>array(0=>1,1=>1,2=>1,4=>1,6=>0,13=>0,),26=>array(0=>1,1=>1,2=>1,4=>0,),33=>array(0=>1,1=>1,2=>1,4=>1,11=>1,12=>1,),38=>array(0=>1,1=>1,2=>1,4=>0,),42=>array(0=>1,1=>1,2=>1,4=>0,),51=>array(0=>1,1=>1,2=>1,4=>0,14=>0,),52=>array(0=>1,1=>1,2=>1,4=>0,),53=>array(0=>1,1=>1,2=>1,4=>0,),55=>array(0=>1,1=>1,2=>1,3=>0,4=>0,),56=>array(0=>1,1=>1,2=>1,3=>0,4=>0,),59=>array(0=>0,1=>0,2=>0,3=>0,4=>0,),);

$current_user_groups=array(50,28,);

$subordinate_roles=array();

$parent_roles=array('H1','H2','H3','H10',);

$subordinate_roles_users=array();

$user_info=array('user_name'=>'Rami','is_admin'=>'off','user_password'=>'$1$Ra000000$ofqN8e2t/mr1I8KwUiyYo.','confirm_password'=>'$1$Ra000000$ofqN8e2t/mr1I8KwUiyYo.','first_name'=>'','last_name'=>'Rami','roleid'=>'H11','email1'=>'Rami_99@gmail.com','status'=>'Active','activity_view'=>'Today','lead_view'=>'Today','hour_format'=>'24','end_hour'=>'','start_hour'=>'00:00','title'=>'','phone_work'=>'','department'=>'','phone_mobile'=>'','reports_to_id'=>'','phone_other'=>'','email2'=>'','phone_fax'=>'','secondaryemail'=>'','phone_home'=>'','date_format'=>'dd-mm-yyyy','signature'=>'','description'=>'','address_street'=>'','address_city'=>'','address_state'=>'','address_postalcode'=>'','address_country'=>'','accesskey'=>'2DC25fL8jS66VSvD','time_zone'=>'Pacific/Midway','currency_id'=>'1','currency_grouping_pattern'=>'123,456,789','currency_decimal_separator'=>'.','currency_grouping_separator'=>'&nbsp;','currency_symbol_placement'=>'$1.0','imagename'=>'','internal_mailer'=>'0','theme'=>'bluelagoon','language'=>'en_us','reminder_interval'=>'','phone_crm_extension'=>'','no_of_currency_decimals'=>'2','truncate_trailing_zeros'=>'0','dayoftheweek'=>'Monday','callduration'=>'5','othereventduration'=>'5','calendarsharedtype'=>'public','default_record_view'=>'Summary','leftpanelhide'=>'0','rowheight'=>'narrow','defaulteventstatus'=>'Select an Option','defaultactivitytype'=>'Select an Option','hidecompletedevents'=>'0','is_owner'=>'0','smat'=>'','wdata'=>'','currency_name'=>'USA, Dollars','currency_code'=>'USD','currency_symbol'=>'&#36;','conv_rate'=>'1.00000','record_id'=>'','record_module'=>'','id'=>'49');
?>