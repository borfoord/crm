<?php
vimport('~~/vtlib/Vtiger/Module.php');
/**
 * DragNDrop Module Model Class
 */
class DragNDrop_Module_Model extends Vtiger_Module_Model 
{
	/**
	 * Function to get the module is permitted to specific action
	 * @param <String> $actionName
	 * @return <boolean>
	 */
	public function isPermitted($actionName) {
		// restrict user from creating/updating/importing record
		if($actionName == 'EditView' || $actionName == 'Import')
			return false;

		return ($this->isActive() && Users_Privileges_Model::isPermitted($this->getName(), $actionName));
	}

	/**
	 * Function to get Settings links
	 * @return <Array>
	 */
	public function getSettingLinks(){
		if(!$this->isEntityModule()) {
			return array();
		}
		vimport('~~modules/com_vtiger_workflow/VTWorkflowUtils.php');

		$layoutEditorImagePath = Vtiger_Theme::getImagePath('LayoutEditor.gif');
		$editWorkflowsImagePath = Vtiger_Theme::getImagePath('EditWorkflows.png');
		$settingsLinks = array();

		// no need to show settings link
		$settingsLinks = array();
		return $settingsLinks;
	}

	public function isCustomizable()	{
		return true;
    	}

	/**
	 * Function to identify if the module supports quick search or not
     	 */
    	public function isQuickSearchEnabled() {
        	return false;
    	}
}
