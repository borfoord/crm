<?php
require_once('modules/DragNDrop/config.php');
require_once('modules/DragNDrop/helper.php');
class DragNDrop_Settings_View extends Vtiger_Index_View 
{
	function checkPermission(Vtiger_Request $request) {
                $moduleName = $request->getModule();
                $moduleModel = Vtiger_Module_Model::getInstance($moduleName);

                $currentUserPriviligesModel = Users_Privileges_Model::getCurrentUserPrivilegesModel();
                if(!$currentUserPriviligesModel->hasModuleActionPermission($moduleModel->getId(), 'Settings')) {
                        $permission_denied = vtranslate('LBL_PERMISSION_DENIED');
                        throw new AppException("{$permission_denied} to access Settings. Click <a href = 'index.php?module={$moduleName}&view=List&showLogs=true'> here </a> to go to DragNDrop");
                }
        }

	public function preProcess(Vtiger_Request $request, $display = true)    {
                $moduleName = $request->getModule();
                // get sidebar link list
                $sideBarLinks = DragNDropConfig::$sideBarLinks;
                $sideBarLinks[0]['class'] = 'selectedQuickLink';
                $sideBarLinks[0]['sequence'] = 0;

                $viewer = $this->getViewer($request);
		$viewer->assign('MODULE', $moduleName);
                $viewer->assign('DDD_TITLE', DragNDropConfig::$moduleTitle);
                $viewer->assign('DDD_VERSION', DragNDropConfig::$version);

                $viewer->assign('sideBarLinks', $sideBarLinks);
                $viewer->assign('moduleName', $moduleName);
                return parent::preProcess($request, $display);
        }

	protected function preProcessDisplay(Vtiger_Request $request)	{
                parent::preProcessDisplay($request);
        }

	public function process(Vtiger_Request $request)	{
		global $adb;
		$docRelatedModules = $data = array();

		$vtFunctions = new Vtiger_Functions();
		$helper = new DragNDropHelper();

		$moduleName = $request->getModule();
		$viewer = $this->getViewer($request);

		// get documents tabid
		$documentTabId = $vtFunctions->getModuleId('Documents');

		// get settings value
		$serialised_data = $helper->getConfigValue('serialised_data');
		if(!empty($serialised_data))
			$data = unserialize(base64_decode($serialised_data));

		$docRelatedModules = $helper->getDocumentRelatedModules($documentTabId, $vtFunctions);
		foreach($docRelatedModules as $key => $single_module)	{
			$docRelatedModules[$key]['folder_id'] = $data['documentfolders_' . $single_module['moduleName']];
			if(isset($data[$single_module['moduleName']]) && $data[$single_module['moduleName']] == 'on')
				$docRelatedModules[$key]['checked'] = 'checked';

		}

		// get document modules folder
                $document_folders = $helper->getDocumentFolders();

		$viewer->assign('HELPER', $helper);
		$viewer->assign('document_folders', $document_folders);
		$viewer->assign('data', $docRelatedModules);
		$viewer->assign('MODULE', $moduleName);
		$viewer->assign('DDD_SETTINGS_TITLE', DragNDropConfig::$settingsTitle);
		$viewer->assign('version', DragNDropConfig::$version);
		$viewer->view('Settings.tpl', $moduleName);
	}
}
