<?php
require_once('modules/DragNDrop/config.php');
require_once('modules/DragNDrop/helper.php');
class DragNDrop_Widget_View extends Vtiger_Index_View 
{
	function __construct()	{
		parent::__construct();
	}

	function process(Vtiger_Request $request)	{
		global $adb;
		$viewer = $this->getViewer($request);
		$mod = $request->get('mod');
		$record = $request->get('record');
		$moduleName = $request->getModule();

		$viewer->assign('VIEW', $request->get('view'));
		$viewer->assign('SCRIPTS', $this->getHeaderScripts($request));
		$viewer->assign('MOD', $mod);
		$viewer->assign('MODULE', $moduleName);
		$viewer->assign('RECORD', $record);
		$viewer->view('DetailViewWidget.tpl', $moduleName);
	}

	public function getHeaderScripts(Vtiger_Request $request) {
                $headerScriptInstances = parent::getHeaderScripts($request);
                $headerScriptInstances = array();
                $moduleName = $request->getModule();

                $jsFileNames = array(
                        "modules.$moduleName.resources.Custom",
                        "modules.$moduleName.libs.jQueryFileUploader.js.vendor.jquery-ui-widget",
                        "modules.$moduleName.libs.jQueryFileUploader.js.jquery-iframe-transport",
                        "modules.$moduleName.libs.jQueryFileUploader.js.jquery-fileupload",
                        "modules.$moduleName.libs.jQueryFileUploader.js.jquery-fileupload-image",
                        "modules.$moduleName.libs.jQueryFileUploader.js.jquery-fileupload-process",
                        "modules.$moduleName.libs.jQueryFileUploader.js.jquery-fileupload-validate",
                        "modules.$moduleName.libs.jQueryFileUploader.js.main",
                );

                $jsScriptInstances = $this->checkAndConvertJsScripts($jsFileNames);
                $headerScriptInstances = array_merge($headerScriptInstances, $jsScriptInstances);
                return $headerScriptInstances;
        }
}
