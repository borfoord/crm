<?php
class DragNDrop_Incoming_Action extends Vtiger_Action_Controller
{
	public function __construct()	{
		require_once('modules/DragNDrop/helper.php');
		require_once('modules/DragNDrop/libs/jQueryFileUploader/UploadHandler.php');
	}

	public function checkPermission(Vtiger_Request $request)	{

	}

	public function process(Vtiger_Request $request) {
		global $adb, $current_user, $site_URL, $root_directory;
		// getting request values using _SERVER
		// TODO try to get the record id and url without using http_referer method
		$http_referer = $_SERVER['HTTP_REFERER'];
		$parsed_url = parse_url($http_referer);
		$exploded_parsed_url = explode('&', $parsed_url['query']);
		foreach($exploded_parsed_url as $single_parsed_url)	{
			$exploded_single_parsed_url = explode('=', $single_parsed_url);
			if($exploded_single_parsed_url[0] == 'module')
				$module = $exploded_single_parsed_url[1];
			else if($exploded_single_parsed_url[0] == 'record')
				$record = $exploded_single_parsed_url[1];

		}

		$uploadHelper = array();
                $helper = new DragNDropHelper();
		$uploadHelper = new UploadHandlerNatty();

		$count = array();
		// resetting count values
		$count['success'] = $count['failure'] = 0;
		$response = $uploadHelper->response['files'];
		if(is_array($response) && count($response) >= 1)	{
			foreach($response as $single_response)	{
				if(!empty($single_response->error))
					die($single_response->error);

				$doc_url = $single_response->url;
				$doc_dir = str_replace($site_URL, $root_directory, $doc_url);
				$response = $helper->createDoc($doc_dir, $single_response, $record, $module);
			}
		}
		else	{
			$response = '.';
		}
		die($response);
	}
}
