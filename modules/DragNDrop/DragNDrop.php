<?php
/*+**********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 ************************************************************************************/

include_once 'modules/Vtiger/CRMEntity.php';

class DragNDrop extends Vtiger_CRMEntity {
	var $table_name = 'vtiger_dragndrop';
	var $table_index= 'dragndropid';

	/**
	 * Mandatory table for supporting custom fields.
	 */
	var $customFieldTable = Array('vtiger_dragndropcf', 'dragndropid');

	/**
	 * Mandatory for Saving, Include tables related to this module.
	 */
	var $tab_name = Array('vtiger_crmentity', 'vtiger_dragndrop', 'vtiger_dragndropcf');

	/**
	 * Mandatory for Saving, Include tablename and tablekey columnname here.
	 */
	var $tab_name_index = Array(
		'vtiger_crmentity' => 'crmid',
		'vtiger_dragndrop' => 'dragndropid',
		'vtiger_dragndropcf'=>'dragndropid');

	/**
	 * Mandatory for Listing (Related listview)
	 */
	var $list_fields = Array (
		/* Format: Field Label => Array(tablename, columnname) */
		// tablename should not have prefix 'vtiger_'
		'Module Name' => Array('dragndrop', 'name'),
		'Assigned To' => Array('crmentity','smownerid')
	);
	var $list_fields_name = Array (
		/* Format: Field Label => fieldname */
		'Module Name' => 'name',
		'Assigned To' => 'assigned_user_id',
	);

	// Make the field link to detail view
	var $list_link_field = 'modulename';

	// For Popup listview and UI type support
	var $search_fields = Array(
		/* Format: Field Label => Array(tablename, columnname) */
		// tablename should not have prefix 'vtiger_'
		'Module Name' => Array('dragndrop', 'name'),
		'Assigned To' => Array('vtiger_crmentity','assigned_user_id'),
	);
	var $search_fields_name = Array (
		/* Format: Field Label => fieldname */
		'Module Name' => 'name',
		'Assigned To' => 'assigned_user_id',
	);

	// For Popup window record selection
	var $popup_fields = Array ('name');

	// For Alphabetical search
	var $def_basicsearch_col = 'name';

	// Column value to use on detail view record text display
	var $def_detailview_recname = 'name';

	// Used when enabling/disabling the mandatory fields for the module.
	// Refers to vtiger_field.fieldname values.
	var $mandatory_fields = Array('name','assigned_user_id');

	var $default_order_by = 'name';
	var $default_sort_order='ASC';

	/**
	* Invoked when special actions are performed on the module.
	* @param String Module name
	* @param String Event Type
	*/
	function vtlib_handler($moduleName, $eventType) {
		global $adb;
 		if($eventType == 'module.postinstall') {
			require_once('modules/DragNDrop/config.php');
                        $version = DragNDropConfig::$version;

			$adb->pquery('insert into vtiger_ddd_settings (config_name) values (?)', array('serialised_data'));
			$adb->pquery('insert into vtiger_ddd_settings (config_name, config_value) values (?, ?)', array('version', $version));

			// check whether settings already added into the action mapping
			$checkSettings = $adb->pquery('select actionid from vtiger_actionmapping where actionname = ?', array('Settings'));
			$settings_count = $adb->num_rows($checkSettings);
			if($settings_count == 0)	{
				// adding new action "Settings"
				$maxActionIdResult = $adb->pquery('SELECT MAX(actionid) AS maxid FROM vtiger_actionmapping', array());
				$maxActionId = $adb->query_result($maxActionIdResult, 0, 'maxid');
				$adb->pquery('insert into vtiger_actionmapping values (?, ?, 1)', array($maxActionId + 1, 'Settings'));
			}

			// enable settings tool
			$module = Vtiger_Module_Model::getInstance($moduleName);
			$module->enableTools(Array('Settings'));
		} 
		else if($eventType == 'module.disabled') {
			// TODO Handle actions before this module is being uninstalled.
		} else if($eventType == 'module.preuninstall') {
			// TODO Handle actions when this module is about to be deleted.
		} else if($eventType == 'module.preupdate') {
			// TODO Handle actions before this module is updated.
		} 
		else if($eventType == 'module.postupdate') {
			require_once('modules/DragNDrop/config.php');
                        $version = DragNDropConfig::$version;
			if($version == 1.1)	{
				$adb->pquery('update vtiger_ddd_settings set config_value = ? where config_name = ?', array($version, 'version'));
			}
			else if($version == 1.2)        {
				// check whether settings already added into the action mapping
				$checkSettings = $adb->pquery('select actionid from vtiger_actionmapping where actionname = ?', array('Settings'));
				$settings_count = $adb->num_rows($checkSettings);
				if($settings_count == 0)	{
	                                // adding new action "Settings"
        	                        $maxActionIdResult = $adb->pquery('SELECT MAX(actionid) AS maxid FROM vtiger_actionmapping', array());
                	                $maxActionId = $adb->query_result($maxActionIdResult, 0, 'maxid');
                        	        $adb->pquery('insert into vtiger_actionmapping values (?, ?, 1)', array($maxActionId + 1, 'Settings'));
				}

                                // enable settings tool
				$module = Vtiger_Module_Model::getInstance($moduleName);
                                $module->enableTools(Array('Settings'));
                        }
		}
 	}
}
