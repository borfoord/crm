<?php
/**
 * DragNDrop config
 * @author Nattyant
 */
class DragNDropConfig	{
	/**
	 * Settings page title
	 */
	public static $settingsTitle = 'LBL_DDD_SETTINGS';

	/**
	 * Module Page Title
	 */
	public static $moduleTitle = 'LBL_DDD_TITLE';

	/**
	 * Module version
	 */
	public static $version = '1.2.1';

	/**
         * side bar link list
         */
        public static $sideBarLinks = array(
                                        0 => array('name' => 'LBL_DRAGNDROP_SETTINGS', 'link' => 'index.php?module=DragNDrop&view=Settings', 'class' => 'unselectedQuickLink'),
                                        1 => array('name' => 'LBL_DRAGNDROP_LOGS', 'link' => 'index.php?module=DragNDrop&view=List&showLogs=true', 'class' => 'unselectedQuickLink')
                                );
}
