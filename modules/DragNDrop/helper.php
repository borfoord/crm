<?php
class DragNDropHelper
{
	/**
         * return config value from settings table
         * @param string $configName
         * @return string $configValue
         */
        public function getConfigValue($configName)     {
                global $adb;
                $getConfigValue = $adb->pquery('select config_value from vtiger_ddd_settings where config_name = ?', array($configName));
                $configValue = $adb->query_result($getConfigValue, 0, 'config_value');
                return $configValue;
        }

	/**
         * return document folders
         * @return array $folders - Documents Module folders
         */
        public function getDocumentFolders()    {
                $fieldObj = new Vtiger_Field_Model();
                $folders = $fieldObj->getDocumentFolders();
                return $folders;
        }

	/**
         * return common config value from settings table
         * @param string $configName
         * @return string $configValue
         */
        public function getCommonConfigValue($configName)       {
                global $adb;
                $getConfigValue = $adb->pquery('select config_value from vtiger_ddd_settings where config_name = ?', array($configName));
                $configValue = $adb->query_result($getConfigValue, 0, 'config_value');
                return $configValue;
        }

	/**
         * return folder id for given module name
         * @param string $module_name - module name
         * @return interger $folder_id - documents folder id
         */
        public function getDocumentFolderId($module_name)       {
                global $adb, $current_user; $data = array();
                // get settings value
                $serialised_data = $this->getCommonConfigValue('serialised_data');
                if(!empty($serialised_data))
                        $data = unserialize(base64_decode($serialised_data));

                return $data['documentfolders_' . $module_name];
        }

	/**
         * creating new widget for DragNDrop in given module
         * @param string $module
         */
        public function createWidget($module)   {
                // creating widget on documents module
                $modInstance = Vtiger_Module::getInstance($module);
                if($modInstance)
                        $modInstance->addLink("DETAILVIEWSIDEBARWIDGET", "Drag N Drop", "module=DragNDrop&view=Widget&viewtype=detail&mod=" . $module);

        }

        /**
         * deleting DragNDrop widget in given module
         * @param string $module
         */
        public function deleteWidget($module)   {
                // deleting widget on module
                $modInstance = Vtiger_Module::getInstance($module);
                if($modInstance)
                        $modInstance->deleteLink("DETAILVIEWSIDEBARWIDGET", "Drag N Drop", "module=DragNDrop&view=Widget&viewtype=detail&mod=" . $module);

        }
	
	/**
	 * get document related modules
	 * @param integer $documentTabId
	 * @param object $vtFunctions
	 * @param array $docRelatedModules
	 */
	public function getDocumentRelatedModules($documentTabId, $vtFunctions)	{
		global $adb; $count = 0;
		$not_needed_modules = array('Emails');
		$docRelatedModules = array();
		$getDocRelatedModules = $adb->pquery('select tabid from vtiger_relatedlists where related_tabid = ?', array($documentTabId));
                $count = $adb->num_rows($getDocRelatedModules);
                for($i = 0; $i < $count; $i ++) {
                        $docRelatedModules[$i]['tabid'] = $adb->query_result($getDocRelatedModules, $i, 'tabid');
                        $docRelatedModules[$i]['moduleName'] = $vtFunctions->getModuleName($docRelatedModules[$i]['tabid']);
                        $docRelatedModules[$i]['checked'] = null;
			if(in_array($docRelatedModules[$i]['moduleName'], $not_needed_modules))
				unset($docRelatedModules[$i]);

                }
		return $docRelatedModules;
	}

	/**
         * create new document in vtiger
         * @param string $filePath
         * @param array $fileInfo
	 * @param integer $rel_id
	 * @param string $rel_module
         * @return string $response
         */
        public function createDoc($filePath, $fileInfo, $rel_id, $rel_module) {
                global $current_user, $upload_badext, $adb;

		$log_data = array();
		$log_data['name'] = $rel_module;
		$filePath = rawurldecode($filePath);
                $tmp_fileInfo = pathinfo($filePath);
                $fileInfo->filename = $tmp_fileInfo['filename'];
		$fileInfo->basename = $tmp_fileInfo['basename'];

		if($_FILES['files'])	{
			foreach($_FILES['files'] as $attr => $attr_val)	{
				$_FILES['files'][$attr] = $attr_val[0];
			}
		}

		$modePermission = Users_Privileges_Model::isPermitted('Documents', 'EditView');
		if($modePermission != true)	{
			$response = getTranslatedString('LBL_PERMISSION_DENIED');
			$log_data['message'] = $response;
			$log_data['result'] = 'Failed';
			$this->addLog($log_data);
			// removing file from cache folder after uploaded to vtiger storage directory
                        unlink($filePath);
			return 'LBL_PERMISSION_DENIED';
		}

		$folderid = $this->getDocumentFolderId($rel_module);
                // In case folder id is empty, set foldername to Default
                if(empty($folderid) || $folderid == 0)
                        $folderid = 1;

		$response = array();
                require_once('modules/Documents/Documents.php');
                $doc = new Documents();
                $doc->mode = 'create'; $doc->id = null;
                $binFile = sanitizeUploadFileName($fileInfo->basename, $upload_badext);
                $filename = ltrim(basename(" ".$binFile));
                $doc->column_fields['notes_title'] = $filename;
                $doc->column_fields['filename'] = $filename;
                $doc->column_fields['filetype'] = $fileInfo->type;
                $doc->column_fields['filesize'] = $fileInfo->size;
                $doc->column_fields['filelocationtype'] = 'I';
                $doc->column_fields['filestatus'] = 1;
                $doc->column_fields['folderid'] = $folderid;
                $doc->column_fields['assigned_user_id'] = $current_user->id;
                $doc->save('Documents');
                if($doc->id)    {
                        $entityObj = new CRMEntity();
                        //$response = $entityObj->uploadAndSaveFile($doc->id, 'Documents', $_FILES['files']);
			// removing file from cache folder after uploaded to vtiger storage directory
			if(file_exists($filePath))
				unlink($filePath);

			// relating record to module 
			$adb->pquery('insert into vtiger_senotesrel (crmid, notesid) values (?, ?)', array($rel_id, $doc->id));
			$response = "{$doc->column_fields['notes_title']} created and related successfully";
			$log_data['message'] = $response;
			$log_data['result'] = 'Success';
			$this->addLog($log_data);

			if(vtlib_isModuleActive('ModTracker')) {
				// Track the time the relation was added
				require_once 'modules/ModTracker/ModTracker.php';
				ModTracker::linkRelation($rel_module, $rel_id, 'Documents', $doc->id);
			}
                }
                return $response;
        }

	/**
         * add dragNdrop log
         * @param array $data
         */
        public function addLog($data)   {
                global $current_user;
                require_once('modules/DragNDrop/DragNDrop.php');
                $log = new DragNDrop();
                $log->id = null;
                $log->mode = 'create';
                $log->column_fields['name'] = $data['name'];
                $log->column_fields['message'] = $data['message'];
                $log->column_fields['result'] = $data['result'];
                $log->column_fields['assigned_user_id'] = $current_user->id;
                $log->save('DragNDrop');
        }
}
