<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/

class Leads_Record_Model extends Vtiger_Record_Model {

	/**
	 * Function returns the url for converting lead
	 */
	function getConvertLeadUrl() {
		return 'index.php?module='.$this->getModuleName().'&view=ConvertLead&record='.$this->getId();
	}

	/**
	 * Static Function to get the list of records matching the search key
	 * @param <String> $searchKey
	 * @return <Array> - List of Vtiger_Record_Model or Module Specific Record Model instances
	 */
	public static function getSearchResult($searchKey, $module=false) {
		$db = PearDatabase::getInstance();

		$deletedCondition = $this->getModule()->getDeletedRecordCondition();
		$query = 'SELECT * FROM vtiger_crmentity
                    INNER JOIN vtiger_leaddetails ON vtiger_leaddetails.leadid = vtiger_crmentity.crmid
                    WHERE label LIKE ? AND '.$deletedCondition;
		$params = array("%$searchKey%");
		$result = $db->pquery($query, $params);
		$noOfRows = $db->num_rows($result);

		$moduleModels = array();
		$matchingRecords = array();
		for($i=0; $i<$noOfRows; ++$i) {
			$row = $db->query_result_rowdata($result, $i);
			$row['id'] = $row['crmid'];
			$moduleName = $row['setype'];
			if(!array_key_exists($moduleName, $moduleModels)) {
				$moduleModels[$moduleName] = Vtiger_Module_Model::getInstance($moduleName);
			}
			$moduleModel = $moduleModels[$moduleName];
			$modelClassName = Vtiger_Loader::getComponentClassName('Model', 'Record', $moduleName);
			$recordInstance = new $modelClassName();
			$matchingRecords[$moduleName][$row['id']] = $recordInstance->setData($row)->setModuleFromInstance($moduleModel);
		}
		return $matchingRecords;
	}

	/**
	 * Function returns Account fields for Lead Convert
	 * @return Array
	 */
	function getAccountFieldsForLeadConvert() {
		$accountsFields = array();
		$privilegeModel = Users_Privileges_Model::getCurrentUserPrivilegesModel();
		$moduleName = 'Accounts';

		if(!Users_Privileges_Model::isPermitted($moduleName, 'EditView')) {
			return;
		}

		$moduleModel = Vtiger_Module_Model::getInstance($moduleName);
		if ($moduleModel->isActive()) {
			$fieldModels = $moduleModel->getFields();
            //Fields that need to be shown
            $complusoryFields = array('industry');
			foreach ($fieldModels as $fieldName => $fieldModel) {
				if($fieldModel->isMandatory() && $fieldName != 'assigned_user_id') {
                    $keyIndex = array_search($fieldName,$complusoryFields);
                    if($keyIndex !== false) {
                        unset($complusoryFields[$keyIndex]);
                    }
					$leadMappedField = $this->getConvertLeadMappedField($fieldName, $moduleName);
					$fieldModel->set('fieldvalue', $this->get($leadMappedField));
					$accountsFields[] = $fieldModel;
				}
			}
            foreach($complusoryFields as $complusoryField) {
                $fieldModel = Vtiger_Field_Model::getInstance($complusoryField, $moduleModel);
				if($fieldModel->getPermissions('readwrite')) {
                    $industryFieldModel = $moduleModel->getField($complusoryField);
                    $industryLeadMappedField = $this->getConvertLeadMappedField($complusoryField, $moduleName);
                    $industryFieldModel->set('fieldvalue', $this->get($industryLeadMappedField));
                    $accountsFields[] = $industryFieldModel;
                }
            }
		}
		return $accountsFields;
	}

	/**
	 * Function returns Contact fields for Lead Convert
	 * @return Array
	 */
	function getContactFieldsForLeadConvert() {
		$contactsFields = array();
		$privilegeModel = Users_Privileges_Model::getCurrentUserPrivilegesModel();
		$moduleName = 'Contacts';

		if(!Users_Privileges_Model::isPermitted($moduleName, 'EditView')) {
			return;
		}

		$moduleModel = Vtiger_Module_Model::getInstance($moduleName);
		if ($moduleModel->isActive()) {
			$fieldModels = $moduleModel->getFields();
            $complusoryFields = array('firstname', 'email');
            foreach($fieldModels as $fieldName => $fieldModel) {
                if($fieldModel->isMandatory() &&  $fieldName != 'assigned_user_id' && $fieldName != 'account_id') {
                    $keyIndex = array_search($fieldName,$complusoryFields);
                    if($keyIndex !== false) {
                        unset($complusoryFields[$keyIndex]);
                    }

                    $leadMappedField = $this->getConvertLeadMappedField($fieldName, $moduleName);
                    $fieldValue = $this->get($leadMappedField);
                    if ($fieldName === 'account_id') {
                        $fieldValue = $this->get('company');
                    }
                    $fieldModel->set('fieldvalue', $fieldValue);
                    $contactsFields[] = $fieldModel;
                }
            }

			foreach($complusoryFields as $complusoryField) {
                $fieldModel = Vtiger_Field_Model::getInstance($complusoryField, $moduleModel);
				if($fieldModel->getPermissions('readwrite')) {
					$leadMappedField = $this->getConvertLeadMappedField($complusoryField, $moduleName);
					$fieldModel = $moduleModel->getField($complusoryField);
					$fieldModel->set('fieldvalue', $this->get($leadMappedField));
					$contactsFields[] = $fieldModel;
				}
			}
		}
		return $contactsFields;
	}

	/**
	 * Function returns Potential fields for Lead Convert
	 * @return Array
	 */
	function getPotentialsFieldsForLeadConvert() {
		$potentialFields = array();
		$privilegeModel = Users_Privileges_Model::getCurrentUserPrivilegesModel();
		$moduleName = 'Potentials';

		if(!Users_Privileges_Model::isPermitted($moduleName, 'EditView')) {
			return;
		}

		$moduleModel = Vtiger_Module_Model::getInstance($moduleName);
		if ($moduleModel->isActive()) {
			$fieldModels = $moduleModel->getFields();

            $complusoryFields = array('amount');
			foreach($fieldModels as $fieldName => $fieldModel) {
				if($fieldModel->isMandatory() &&  $fieldName != 'assigned_user_id' && $fieldName != 'related_to'
						&& $fieldName != 'contact_id') {
                    $keyIndex = array_search($fieldName,$complusoryFields);
                    if($keyIndex !== false) {
                        unset($complusoryFields[$keyIndex]);
                    }
					$leadMappedField = $this->getConvertLeadMappedField($fieldName, $moduleName);
					$fieldModel->set('fieldvalue', $this->get($leadMappedField));
					$potentialFields[] = $fieldModel;
				}
			}
            foreach($complusoryFields as $complusoryField) {
                $fieldModel = Vtiger_Field_Model::getInstance($complusoryField, $moduleModel);
                if($fieldModel->getPermissions('readwrite')) {
                    $fieldModel = $moduleModel->getField($complusoryField);
                    $amountLeadMappedField = $this->getConvertLeadMappedField($complusoryField, $moduleName);
                    $fieldModel->set('fieldvalue', $this->get($amountLeadMappedField));
                    $potentialFields[] = $fieldModel;
                }
            }
		}
		return $potentialFields;
	}

	/**
	 * Function returns field mapped to Leads field, used in Lead Convert for settings the field values
	 * @param <String> $fieldName
	 * @return <String>
	 */
	function getConvertLeadMappedField($fieldName, $moduleName) {
		$mappingFields = $this->get('mappingFields');

		if (!$mappingFields) {
			$db = PearDatabase::getInstance();
			$mappingFields = array();

			$result = $db->pquery('SELECT * FROM vtiger_convertleadmapping', array());
			$numOfRows = $db->num_rows($result);

			$accountInstance = Vtiger_Module_Model::getInstance('Accounts');
			$accountFieldInstances = $accountInstance->getFieldsById();

			$contactInstance = Vtiger_Module_Model::getInstance('Contacts');
			$contactFieldInstances = $contactInstance->getFieldsById();

			$potentialInstance = Vtiger_Module_Model::getInstance('Potentials');
			$potentialFieldInstances = $potentialInstance->getFieldsById();

			$leadInstance = Vtiger_Module_Model::getInstance('Leads');
			$leadFieldInstances = $leadInstance->getFieldsById();

			for($i=0; $i<$numOfRows; $i++) {
				$row = $db->query_result_rowdata($result,$i);
				if(empty($row['leadfid'])) continue;

				$leadFieldInstance = $leadFieldInstances[$row['leadfid']];
				if(!$leadFieldInstance) continue;

				$leadFieldName = $leadFieldInstance->getName();
				$accountFieldInstance = $accountFieldInstances[$row['accountfid']];
				if ($row['accountfid'] && $accountFieldInstance) {
					$mappingFields['Accounts'][$accountFieldInstance->getName()] = $leadFieldName;
				}
				$contactFieldInstance = $contactFieldInstances[$row['contactfid']];
				if ($row['contactfid'] && $contactFieldInstance) {
					$mappingFields['Contacts'][$contactFieldInstance->getName()] = $leadFieldName;
				}
				$potentialFieldInstance = $potentialFieldInstances[$row['potentialfid']];
				if ($row['potentialfid'] && $potentialFieldInstance) {
					$mappingFields['Potentials'][$potentialFieldInstance->getName()] = $leadFieldName;
				}
			}
			$this->set('mappingFields', $mappingFields);
		}
		return $mappingFields[$moduleName][$fieldName];
	}

	/**
	 * Function returns the fields required for Lead Convert
	 * @return <Array of Vtiger_Field_Model>
	 */
	function getConvertLeadFields() {
		$convertFields = array();
		$accountFields = $this->getAccountFieldsForLeadConvert();
		if(!empty($accountFields)) {
			$convertFields['Accounts'] = $accountFields;
		}

		$contactFields = $this->getContactFieldsForLeadConvert();
		if(!empty($contactFields)) {
			$convertFields['Contacts'] = $contactFields;
		}

		$potentialsFields = $this->getPotentialsFieldsForLeadConvert();
		if(!empty($potentialsFields)) {
			$convertFields['Potentials'] = $potentialsFields;
		}
		return $convertFields;
	}

	/**
	 * Function returns the url for create event
	 * @return <String>
	 */
	function getCreateEventUrl() {
		$calendarModuleModel = Vtiger_Module_Model::getInstance('Calendar');
		return $calendarModuleModel->getCreateEventRecordUrl().'&parent_id='.$this->getId();
	}

	/**
	 * Function returns the url for create todo
	 * @return <String>
	 */
	function getCreateTaskUrl() {
		$calendarModuleModel = Vtiger_Module_Model::getInstance('Calendar');
		return $calendarModuleModel->getCreateTaskRecordUrl().'&parent_id='.$this->getId();
	}
    
    /**
	 * Function to check whether the lead is converted or not
	 * @return True if the Lead is Converted false otherwise.
	 */
    function isLeadConverted() {
        $db = PearDatabase::getInstance();
        $id = $this->getId();
        $sql = "select converted from vtiger_leaddetails where converted = 1 and leadid=?";
        $result = $db->pquery($sql,array($id));
        $rowCount = $db->num_rows($result);
        if($rowCount > 0){
            return true;
        }
        return false;
    }

    function getCampaingsRelTime(){
      $db = PearDatabase::getInstance();
      $id = $this->getId();
      $sql = "SELECT
					c.targetid,c.changedon
				FROM vtiger_modtracker_basic AS b, vtiger_modtracker_relations AS c
				WHERE b.crmid=?
					AND c.id=b.id
					AND c.targetmodule='Campaigns'";
      $result = $db->pquery($sql,array($id));
      $response = array();

      for($i=0; $i<$db->num_rows($result); $i++) {
        $row = $db->query_result_rowdata($result, $i);
        $response[$row["targetid"]] = $row["changedon"];
      }

      return $response;
    }

		function getStatus4lw(){
			$moduleName = $this->getModuleName();
			$relatedModuleName = 'Deposits';
			$parentId = $this->get('id');
			$requestedPage = 1;

			$pagingModel = new Vtiger_Paging_Model();
			$pagingModel->set('page',$requestedPage);

			$relationListView = Vtiger_RelationListView_Model::getInstance($this, $relatedModuleName, $label);
			//return $parentId;
			$models = $relationListView->getEntries($pagingModel);
			$noOfEntries = count($models);
			if($noOfEntries < 1 ){
				return "none";
			}
			$spstatus = "open";
			foreach($models as $model){
				$stf = $model->get("depostatus");
				if( "Approved" == $stf ) {
					$spstatus = "closed";
					break;
				}
				if( "Complete" == $stf ) {
					$spstatus = "closed";
					break;
				}
				//var_dump($stf);exit;
			}


			return $spstatus;
		}

		public function lastOnLine($text=false){

			$class = "none";
			$id = $this->getId();
			$id = vtws_getWebserviceEntityId('Leads', $id);
			//$newRec = Leads_Record_Model::getInstanceById($id);
			$mt4s = $this->searchCRMrel($id,"MT4Accounts");
			if($mt4s["error"]) { return $class; }
			$lo = "2016-01-01 10:00:00";
			foreach($mt4s as $mt4){
				if($mt4['cf_1075'] > $lo ) { $lo = $mt4['cf_1075']; }
			}
			if($text) {
				$dt = new DateTimeField($lo);
			 	return $dt->getDisplayDateTimeValue();
			}
			$now = new DateTime();
			$lo = new DateTime($lo);
			$ago = $now->diff($lo);
			if($ago->days == 0){ $class = "today"; }
			else if($ago->days < 4){ $class = "recently"; }
			return $class;
		}

    public function getCountry($val){
			if(empty($val)){ return "";}
      require_once('libraries/cc/countryNames.php');
      $code = strtolower(countryNames::name2iso($val));
      $str = '<span class="f16"><span class="flag '.$code.'"></span><span>'.$val.'</span></span>';
      //error_log($str);
      return $str;
    }

		public function getTimeOff(){
			require_once('libraries/cc/countryNames.php');
			require_once('libraries/tz/userClock.php');

			$country = $this->get('country');
			$region = $this->get('state');
			$ut = userClock::userTime($country,$region);

			return $ut['off'];
		}

		public function getStatus(){

			$status = $this->get('leadstatus');
			$ret = "";
			if( "Not Contacted" == $status ){
				$ret = "nc";
			}
			elseif( "N-A 1" == $status ){
				$ret = "na1";
			}
			elseif( "N-A 2" == $status ){
				$ret = "na2";
			}
			elseif( "N-A 3" == $status ){
				$ret = "na3";
			}
			elseif( "N-A 4" == $status ){
				$ret = "na4";
			}
			elseif( "N-A 5" == $status ){
				$ret = "na5";
			}
			elseif( "Not Interested" == $status ){
				$ret = "ni";
			}
			elseif( "No Answer" == $status ){
				$ret = "na";
			}
			elseif( "Follow Up" == $status ){
				$ret = "fu";
			}
			elseif( "Follow Up - NA" == $status ){
				$ret = "funa";
			}
			elseif( "Hot" == $status ){
				$ret = "hot";
			}
			elseif( "Wrong Details" == $status ){
				$ret = "wd";
			}
			elseif( "Phone switch off" == $status ){
				$ret = "pof";
			}
			elseif( "Duplicate" == $status ){
				$ret = "pof";
			}
			elseif( "No Money" == $status ){
				$ret = "pof";
			}
			elseif( "Contact in Future" == $status ){
				$ret = "cf";
			}
			elseif( "Language Barrier" == $status ){
				$ret = "cf";
			}
			elseif( "Under Age" == $status ){
				$ret = "cf";
			}
			elseif( "Follow Up - NA5" == $status ){
				$ret = "funa5";
			}


			return $ret;
		}


		protected static $trCache = NULL;
		public function DetailViewHeaderSummary($text=false){

			if(!self::$trCache){
				$id = $this->getId();
				$id = vtws_getWebserviceEntityId('Leads', $id);
				$mt4s = $this->searchCRMrel($id,"MT4Accounts");
				if(empty($mt4s) || $mt4s["error"]) { return false; }

				$req = array();
				foreach($mt4s as $mt4 ){
					$req[] = array("login"=>$mt4["login"]);
				}
				self::$trCache = $this->remotecall(array( "accounts" => $req ) );
				$login = 0;
				foreach($mt4s as $mt4 ){
					$login=$mt4["login"];
					if(isset(self::$trCache->{$login})){
						self::$trCache->{$login}->deposit = $mt4["deposit"];
						self::$trCache->{$login}->withdraw = $mt4["withdraw"];
						self::$trCache->{$login}->mt4group = !$this->islive($mt4["group"]);
						self::$trCache->{$login}->credit = $mt4["bonus"];
						self::$trCache->{$login}->balance = $mt4["balance"];
					}
				}
				$this->storeVolume();
			}
			//error_log(var_export(self::$trCache,true));
			//error_log(var_export($mt4s,true));
			return (array)self::$trCache;
		}

		private function islive($group){

			return strncasecmp($group, "demo", 4) === 0;
		}

		private function storeVolume(){

			if(!self::$trCache){ return; }
			
			$db = PearDatabase::getInstance();
			foreach((array)self::$trCache as $acc => $data){
				$sql = 'UPDATE vtiger_mt4accountscf_base AS b
					JOIN vtiger_mt4accounts AS m
					ON ( b.mt4accountsid = m.mt4accountsid )
			 		SET b.volume = ?
					WHERE m.login=?';
				$params = array(intval($data->vl),$acc);
//error_log(var_export($data,true));
				$result = $db->pquery($sql, $params);
//error_log(var_export($result,true));

			}


		}


		//XXX for yfx
		public function showCVPreview(){

			$ret = '';
			return;
			$cv = $this->getLastDoc();
			var_export( $cv );
			if(isset($cv["error"])){ return; }
			$fdet = $this->getCVfn($cv);

			var_export( $fdet );
		}

		private function getCVfn($cv){

			$docId = vtws_getIdComponents($cv["id"])[1];
			$doc = Vtiger_Record_Model::getInstanceById($docId, "Documents");
			$fdet = $doc->getFileDetails();
			if (!empty ($fdet)) {
        $filePath = $fdet['path'];

        $savedFile = $fdet['attachmentsid']."_".$doc->get('filename');

        if(file_exists($filePath.$savedFile)) {
          $returnValue = $filePath.$savedFile;
        }
				else {
          $returnValue = FALSE;
				}
      }


			return $returnValue;
		}

		private function getLastDoc(){

			$id = $this->getId();
			$id = vtws_getWebserviceEntityId('Leads', $id);

			$docs = $this->searchCRMrel($id,"Documents");
			return end($docs);
		}

		private function searchCRMrel($wsid,$mod){
			global $current_user;
			$record = array();
			try {
				include_once 'include/Webservices/RetrieveRelated.php';
				$record = vtws_retrieve_related($wsid,$mod,$mod,$current_user);

			} catch (WebServiceException $ex) {
				$record["error"] = $ex->getMessage().PHP_EOL;
			}

			return $record;
		}

		private function remotecall($acc){
			$url = "http://reports.mymt4shop.com/retention/genstat.php";
			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_URL,$url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
			curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
			curl_setopt($ch, CURLOPT_USERPWD, "andrew:qq");
			curl_setopt($ch,CURLOPT_POST, count($acc));
			curl_setopt($ch,CURLOPT_POSTFIELDS, http_build_query($acc));

			$result = curl_exec($ch);
			curl_close($ch);

			return json_decode($result);
		}

}
