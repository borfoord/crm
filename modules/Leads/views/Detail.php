<?php

/* +***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 * *********************************************************************************** */

class Leads_Detail_View extends Accounts_Detail_View {


	function process(Vtiger_Request $request) {
		//return parent::process($request);
		$mode = $request->getMode();
		if(empty($mode)){
			$mt4stats = $this->getmt4stats($request);
			$viewer = $this->getViewer($request);
			$viewer->assign('MT4ACCS', $mt4stats);
		}
		return parent::process($request);
	}

	public function showModuleDetailView(Vtiger_Request $request) {

		$recordId = $request->get('record');
		$moduleName = $request->getModule();
		$mode = $request->get('requestMode');

		$this->getRelatedCards($request);

		return parent::showModuleDetailView($request);
	}


	function showModuleBasicView($request) {

		$recordId = $request->get('record');
		$moduleName = $request->getModule();

		$this->getRelatedCards($request);

		return parent::showModuleBasicView($request);

	}


	function getmt4stats(Vtiger_Request $request){
		$moduleName = $request->getModule();
		$parentId = $request->get('record');
		$parentRecordModel = Vtiger_Record_Model::getInstanceById($parentId, $moduleName);
		return $parentRecordModel->DetailViewHeaderSummary();
	}

	function getRelatedCards(Vtiger_Request $request) {
		$moduleName = $request->getModule();
		$relatedModuleName = 'SProcess';
		$parentId = $request->get('record');
		$requestedPage = $request->get('page');
		if(empty($requestedPage)) {
			$requestedPage = 1;
		}

		$pagingModel = new Vtiger_Paging_Model();
		$pagingModel->set('page',$requestedPage);

		$parentRecordModel = Vtiger_Record_Model::getInstanceById($parentId, $moduleName);
		$relationListView = Vtiger_RelationListView_Model::getInstance($parentRecordModel, $relatedModuleName, $label);
		$orderBy = $request->get('orderby');
		$sortOrder = $request->get('sortorder');
		if($sortOrder == 'ASC') {
			$nextSortOrder = 'DESC';
			$sortImage = 'icon-chevron-down';
		} else {
			$nextSortOrder = 'ASC';
			$sortImage = 'icon-chevron-up';
		}
		if(!empty($orderBy)) {
			$relationListView->set('orderby', $orderBy);
			$relationListView->set('sortorder',$sortOrder);
		}
		$models = $relationListView->getEntries($pagingModel);
		$links = $relationListView->getLinks();
		$header = $relationListView->getHeaders();
		$noOfEntries = count($models);

		//error_log(var_export($header,true));

		$relationModel = $relationListView->getRelationModel();
		$relatedModuleModel = $relationModel->getRelationModuleModel();
		$relationField = $relationModel->getRelationField();

		$viewer = $this->getViewer($request);
		$viewer->assign('RELATED_RECORDS' , $models);
		$viewer->assign('PARENT_RECORD', $parentRecordModel);
		$viewer->assign('RELATED_LIST_LINKS', $links);
		$viewer->assign('RELATED_HEADERS', $header);
		$viewer->assign('RELATED_MODULE', $relatedModuleModel);
		$viewer->assign('RELATED_ENTIRES_COUNT', $noOfEntries);
		$viewer->assign('RELATION_FIELD', $relationField);

		//$viewer->assign('MODULE', $moduleName);
		//$viewer->assign('PAGING', $pagingModel);

		//$viewer->assign('ORDER_BY',$orderBy);
		//$viewer->assign('SORT_ORDER',$sortOrder);
		//$viewer->assign('NEXT_SORT_ORDER',$nextSortOrder);
		//$viewer->assign('SORT_IMAGE',$sortImage);
		$viewer->assign('COLUMN_NAME',$orderBy);
		$viewer->assign('IS_R_EDITABLE', $relationModel->isEditable());
		$viewer->assign('IS_R_DELETABLE', $relationModel->isDeletable());

	}

}
