<?php

class Leads_RelatedList_View extends Vtiger_RelatedList_View {

  function getCampaingsRelTime( $request ){
    $moduleName = $request->getModule();
    $parentId = $request->get('record');
    $parentRecordModel = Vtiger_Record_Model::getInstanceById($parentId, $moduleName);
    return $parentRecordModel->getCampaingsRelTime();
  }

  function process(Vtiger_Request $request) {
    //return parent::process($request);
    $label = $request->get('tab_label');
    if( "Campaigns" == $label ){
      $camtimes = $this->getCampaingsRelTime($request);
      $viewer = $this->getViewer($request);
      $viewer->assign('SHOW_CAMTIMES', true);
      $viewer->assign('CAMTIMES', $camtimes);
    }
    return parent::process($request);

  }
}