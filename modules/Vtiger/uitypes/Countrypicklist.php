<?php

class Vtiger_CountryPicklist_UIType extends Vtiger_Base_UIType {

	/**
	 * Function to get the Template name for the current UI Type object
	 * @return <String> - Template Name
	 */
	public function getTemplateName() {
		return 'uitypes/CountryPicklist.tpl';
	}

	public function getDisplayValue($value, $record = false, $recordInstance = false) {
		if(empty($value)){ return ""; }
		$str = Vtiger_Language_Handler::getTranslatedString($value, $this->get('field')->getModuleName());
		require_once('libraries/cc/countryNames.php');
		$code = strtolower(countryNames::name2iso($str));
		$str = '<span class="f16"><span class="flag '.$code.'"></span><span>'.$str.'</span></span>';
		//error_log($str);
		return $str;

	}

	public function getListSearchTemplateName() {
		return 'uitypes/CountryPickListFieldSearchView.tpl';
	}

}