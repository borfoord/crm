<?php

require_once('include/database/PearDatabase.php');

class DepositsSaveEventHandler {

	public function handleEvent($handlerType, $entityData){
		//var_dump($handlerType);

		if($handlerType == 'vtiger.entity.beforesave.modifiable') {

			$CountUsersDeposits = 1;

			$moduleName = $entityData->getModuleName();
			$data = $entityData->getData();

			$CheckStatuses = array( "Approved","Complete" );

			$status = $entityData->get("depostatus");
			$userId = $entityData->get("useraccount");

			if (in_array($status,$CheckStatuses) and $userId > 0 ) {
				$db = PearDatabase::getInstance(); 
				$sql = "
					SELECT COUNT(1)
 					FROM vtiger_deposits t1,vtiger_depositscf t2
				 	WHERE t1.depositsid = t2.depositsid
	 					AND t2.ftd = 1
					 	AND useraccount=?";

				$result = $db->pquery($sql,array($userId));
				$row = $db->query_result_rowdata($result, 0);
				$CountUsersDeposits = $row[0];
				if( !$CountUsersDeposits )
					$entityData->set("ftd",1);
			}
		}
	}
}