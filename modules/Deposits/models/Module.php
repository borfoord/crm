<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/

class Deposits_Module_Model extends Vtiger_Module_Model {

	public function getDepovsWithdraw($owner,$dateFilter) {
		$db = PearDatabase::getInstance();

		$ownerSql = $this->getOwnerWhereConditionForDashBoards($owner);
		if(!empty($ownerSql)) {
			$ownerSql = ' AND '.$ownerSql;
		}
		
		$params = array();
		if(!empty($dateFilter)) {
			$dateFilterSql = ' AND entity.createdtime BETWEEN ? AND ? ';
			//client is not giving time frame so we are appending it
			$params[] = $dateFilter['start']. ' 00:00:00';
			$params[] = $dateFilter['end']. ' 23:59:59';
		}

		$sql = ('SELECT SUM(depo.amount) as amount,COUNT(*) AS count, depo.amount > 0 AS dow FROM vtiger_deposits as depo, vtiger_crmentity as entity WHERE depostatus IN ("Complete","Approved") '
						.Users_Privileges_Model::getNonAdminAccessControlQuery($this->getName()). $ownerSql .' '.$dateFilterSql.
						' AND depo.depositsid = entity.crmid '.
						'GROUP BY depo.amount > 0');

		$result = $db->pquery($sql, $params);

		//error_log($sql);
		//error_log(var_export($params,true));

		$response = array();
		
		for($i=0; $i<$db->num_rows($result); $i++) {
			$row = $db->query_result_rowdata($result, $i);
			$response[$i][0] = abs($row['amount']);
			$response[$i]['count'] = abs($row['amount']);
			$response[$i][1] = "sss".$row['dow'];
			$response[$i]['name'] = "sss".$row['dow'];
			$response[$i][2] = abs($row['count']);
		}
		return $response;
	}

}