<?php

include_once 'modules/Vtiger/CRMEntity.php';

class Deposits extends Vtiger_CRMEntity {
	var $table_name = 'vtiger_deposits';
	var $table_index= 'depositsid';

	var $customFieldTable = Array('vtiger_depositscf', 'depositsid');

	var $tab_name = Array('vtiger_crmentity', 'vtiger_deposits', 'vtiger_depositscf');

	var $tab_name_index = Array(
			'vtiger_crmentity' => 'crmid',
			'vtiger_deposits' => 'depositsid',
			'vtiger_depositscf'=>'depositsid');

	var $list_fields = Array (
			'client_orderid' => Array('deposits', 'client_orderid'),
			'Ammount' => Array('deposits', 'amount'),
			'Currency' => Array('deposits', 'currency'),
			'Email' => Array('deposits', 'email'),
			'MT4Account' => Array('deposits', 'mt4account'),
			);

	var $list_fields_name = Array (
			'client_orderid' => 'client_orderid',
			'Ammount' => 'amount',
			'Currency' => 'currency',
			'Email' => 'email',
			'MT4Account' => 'mt4account',
			);

	var $list_link_field = 'client_orderid';

	var $search_fields = Array(
			'client_orderid' => Array('deposits', 'client_orderid'),
			'Ammount' => Array('deposits', 'amount'),
			'Currency' => Array('deposits', 'currency'),
			'Email' => Array('deposits', 'email'),
			'Assigned To' => Array('vtiger_crmentity','assigned_user_id'),
			);

	var $search_fields_name = Array (
			/* Format: Field Label => fieldname */
			'client_orderid' => 'client_orderid',
			'Ammount' => 'amount',
			'Currency' => 'currency',
			'Email' => 'email',
			'Assigned To' => 'assigned_user_id',
			);

	// For Popup window record selection
	var $popup_fields = Array ('client_orderid');

	var $def_basicsearch_col = 'client_orderid';

	var $def_detailview_recname = 'client_orderid';

	var $mandatory_fields = Array('client_orderid','assigned_user_id');

	var $default_sort_order='DESC';

  var $default_order_by = 'createdtime';


}