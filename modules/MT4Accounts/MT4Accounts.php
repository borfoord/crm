<?php

include_once 'modules/Vtiger/CRMEntity.php';

class MT4Accounts extends Vtiger_CRMEntity {
	var $table_name = 'vtiger_mt4accounts';
	var $table_index= 'mt4accountsid';

	var $customFieldTable = Array('vtiger_mt4accountscf', 'mt4accountsid');

	var $tab_name = Array('vtiger_crmentity', 'vtiger_mt4accounts', 'vtiger_mt4accountscf');

	var $tab_name_index = Array(
			'vtiger_crmentity' => 'crmid',
			'vtiger_mt4accounts' => 'mt4accountsid',
			'vtiger_mt4accountscf'=>'mt4accountsid');

	var $list_fields = Array (
			'MT4Account ID'=>Array('crmentity', 'crmid'),
			'Login' => Array('mt4accounts', 'login'),
			'MT4 Group' => Array('mt4accounts', 'mt4group'),
			'MT4 Name' => Array('mt4accounts', 'mt4name'),
			'Balance' => Array('mt4accountscf', 'balance'),
			'Assigned To' => Array('crmentity','smownerid')
			);

	var $list_fields_name = Array (
			'MT4Account ID'=>'crmid',
			'Login' => 'login',
			'MT4 Group' => 'mt4group',
			'MT4 Name' => 'name',
			'Balance' => 'balance',
			'Assigned To' => 'assigned_user_id',
			);

	var $list_link_field = 'login';

	var $search_fields = Array(
			'MT4Account ID'=>Array('vtiger_crmentity', 'crmid'),
			'Login' => Array('mt4accounts', 'login'),
			'MT4 Group' => Array('mt4accounts', 'mt4group'),
			'MT4 Name' => Array('mt4accounts', 'mt4name'),
			'Balance' => Array('mt4accountscf', 'balance'),
			'Assigned To' => Array('vtiger_crmentity','assigned_user_id'),
			);

	var $search_fields_name = Array (
			/* Format: Field Label => fieldname */
			'MT4Account ID'=>'crmid',
			'Login' => 'login',
			'MT4 Group' => 'mt4group',
			'MT4 Name' => 'name',
			'Balance' => 'balance',
			'Assigned To' => 'assigned_user_id',
			);

	// For Popup window record selection
	var $popup_fields = Array ('login','name','mt4group','balance');

	var $def_basicsearch_col = 'login';

	var $def_detailview_recname = 'login';

	var $mandatory_fields = Array('login','assigned_user_id');

	var $default_sort_order='DESC';

  var $default_order_by = 'modifiedtime';

}