jQuery.Class("MailNotification", {

	__init: function(e)	{

		var params = {
                	'module'        : 'MailBadge',
                	'action'        : 'GetMailBadgeStatus',
        	}

        	// get mailbadge status
		AppConnector.request(params).then(
                function(data) {
                        if(data.result.status == 'show')      {
				// load css file
				$('head').append('<link rel="stylesheet" href="modules/MailBadge/public/css/font-awesome.css" type = "text/css" />');
                		// adding notification icon at the header
                		jQuery('#headerLinksBig').prepend("<span class = 'dropdown span settingIcons' onclick = 'redirectToMailManager()'> <span class='fa-stack'> <i class = 'fa fa-envelope-o fa-stack' style = 'font-size: 15px; color: #fff; font-weight: bold;'> </i> <strong class='fa-stack-1x mail_notification_count' id = 'mail_notification_count'> </strong> </span> </span>");
                                getMailCount(data.result.color);
                        }
                });
	}
});

function redirectToMailManager()	{
	window.open('index.php?module=MailManager&view=List', '_blank');
}

function getMailCount(color)	{
	var params = {
                'module'        : 'MailBadge',
                'action'        : 'GetMailCount',
		'_operationarg'	: 'open',
		'_folder'	: 'INBOX',
		'_operation'	: 'folder',
        }

        // get related documents
        AppConnector.request(params).then(
                function(data) {
			var unread = data.result.unread;
			if(unread > 9999)	
				unread = '9999+';

			if(unread == 0)	{
				// no need to show badge when count is 0	
			}
			else if(unread >= 1)	{
				jQuery('#mail_notification_count').css('background-color', color);
				jQuery('#mail_notification_count').show();
				jQuery('#mail_notification_count').html(unread);
			}
			else	{
				jQuery('#mail_notification_count').css('background-color', color);
				jQuery('#mail_notification_count').show();
				jQuery('#mail_notification_count').html('<i class = "fa fa-exclamation" style = "font-size: 10px; vertical-align: middle"> </i>');
			}
                }
        );
}

/**
 *  close color box  
 */
function closeColorBox()	{

}

jQuery('document').ready(function()     {
        var MailNotificationInstance = new MailNotification;
        MailNotificationInstance.__init();
});


