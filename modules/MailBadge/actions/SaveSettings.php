<?php
class MailBadge_SaveSettings_Action extends Vtiger_Action_Controller
{
	public function checkPermission(Vtiger_Request $request)        {

        }

	public function process(Vtiger_Request $request) {
		global $adb, $current_user;
		$data = $_REQUEST;
		$moduleName = $request->getModule();
		// remove unwanted fields from the request
		$unwanted_fields = array('module', 'action', '__vtrftk', 'PHPSESSID');
		foreach($unwanted_fields as $single_unwanted_field)	{
			unset($data[$single_unwanted_field]);
		}

		// store the information in settings table
		$serialized_value = base64_encode(serialize($data));
		$getSettings = $adb->pquery('select * from vtiger_mailbadge_settings where id = ?', array($current_user->id));
		$count = $adb->num_rows($getSettings);
		if($count == 0)
			$adb->pquery('insert into vtiger_mailbadge_settings (id, value) values (?, ?)', array($current_user->id, $serialized_value));
		else
			$adb->pquery('update vtiger_mailbadge_settings set value = ? where id = ?', array($serialized_value, $current_user->id));

		// redirect the user to list view
		header("Location: index.php?module={$moduleName}&view=List&saved=true"); die;
	}
}
