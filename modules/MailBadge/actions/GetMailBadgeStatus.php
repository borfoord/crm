<?php
class MailBadge_GetMailBadgeStatus_Action extends Vtiger_Action_Controller
{
	public function checkPermission(Vtiger_Request $request)        {

        }

	public function process(Vtiger_Request $request) {
		global $adb, $current_user;
		$moduleName = $request->getModule();
		require_once("modules/{$moduleName}/config.php");

		$result = null; 
		$color = MailBadgeConfig::$default_color;

		$getValue = $adb->pquery('select value from vtiger_mailbadge_settings where id = ?', array($current_user->id));
		$serialized_value = $adb->query_result($getValue, 0, 'value');
		if(empty($serialized_value))	{
			$result = 'show';
		}
		else	{
			$unserialized_value = unserialize(base64_decode($serialized_value));
			$color = !empty($unserialized_value['badge_color']) ? $unserialized_value['badge_color'] : $color;
			if($unserialized_value['badge_status'] == 'on')
				$result = 'show';

		}
		$response = new Vtiger_Response();
		$response->setResult(array('status' => $result, 'color' => $color));
		$response->emit();
	}
}
