<?php
class MailBadge_List_View extends Vtiger_Index_View	
{	
	public function process(Vtiger_Request $request)	{
		global $adb, $current_user;
		$viewer = $this->getViewer ($request);
		$moduleName = $request->getModule();
		require_once("modules/{$moduleName}/config.php");

		// getting information from settings table
		$getSettingsValue = $adb->pquery('select value from vtiger_mailbadge_settings where id = ?', array($current_user->id));
		$serialized_value = $adb->query_result($getSettingsValue, 0, 'value');
		if(empty($serialized_value))	{
			// assign default values
			$viewer->assign('BADGE_COLOR', MailBadgeConfig::$default_color);
			$viewer->assign('BADGE_STATUS', 'checked');
		}
		else	{
			// assing values from table
			$unserialized_value = unserialize(base64_decode($serialized_value));
			$viewer->assign('BADGE_COLOR', $unserialized_value['badge_color']);
			if($unserialized_value['badge_status'] == 'on')
	                        $viewer->assign('BADGE_STATUS', 'checked');

		}
		
		$viewer->assign('MODULE_NAME', $moduleName);
		$viewer->view('MailBadgeSettings.tpl', $moduleName);
	}
}
