<?php
    function UpdateStatusPhone($ws_entity){
        // WS id
        $ws_id = $ws_entity->getId();
        $module = $ws_entity->getModuleName();
        if (empty($ws_id) || empty($module)) {
            return;
        }
    
        // CRM id
        $crmid = vtws_getCRMEntityId($ws_id);
        if ($crmid <= 0) {
            return;
        }
    
        //получение объекта со всеми данными о текущей записи Модуля "MyModule"
        $myModuleInstance = Vtiger_Record_Model::getInstanceById($crmid);    
    
        //получение id Заказа
        $soId = $myModuleInstance->get('phone');

        require_once '/libraries/twilio-php-master/Twilio/autoload.php';

            use Twilio\Rest\Client;

            // Your Account Sid and Auth Token from twilio.com/user/account
            $sid = "AC8f9c165d32cb28db4eabfa6c544cd399";
            $token = "53e09418204dd23d59d81519a5e65fac";

            $client = new Client($sid, $token);

            $number = $client->lookups
                ->phoneNumbers($soId)
                ->fetch(
                    array("type" => "carrier")
                );

            $x = $number->carrier["type"] . "\r\n";
            $x = $number->carrier["name"];
    
        if($soId) {
            //получение Даты текущей записи Модуля "MyModule"
            $date = $myModuleInstance->get('date');
        
            //получение объекта со всеми данными о текущей записи Модуля "Заказ"        
            $soInstance = Vtiger_Record_Model::getInstanceById($soId);  
        
            //объект в режиме редактирования          
            $soInstance->set('mode', 'edit');
        
            //запись Даты в поле “Срок оплаты”
            $soInstance->set('duedate', $date);

            //сохранение
            $soInstance->save();
        }
    }