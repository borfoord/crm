<?php

require_once 'vtlib/Vtiger/Net/Client.php';
define('MY_URL','https://my.ibrokers.trade/sign-up/');

class CustomLeads_myZone_Action extends Vtiger_BasicAjax_Action {

    function __construct() {
    parent::__construct();
    $this->exposeMethod('testmail');
    $this->exposeMethod('setpass');
  }


  public function process(Vtiger_Request $request) {
    $mode = $request->getMode();

    if(!empty($mode)) {
      $result = $this->invokeExposedMethod($mode, $request);
    }

    $response = new Vtiger_Response();
    $response->setResult($result);
    $response->emit();

  }

  public function testmail(Vtiger_Request $request) {
    $result = $this->prepare($request, "testmail");
    return $result;
  }

  public function setpass(Vtiger_Request $request) {
    if(!empty($request->get('pass'))){
      return $this->prepare($request, "setpass");
    }
    return false;
  }

  private function prepare(Vtiger_Request $request,$mode) {
    $recordId = $request->get('recordId');
    $moduleName = $request->getModule();

    $moduleModel = Vtiger_Module_Model::getInstance($moduleName);

    $recordModel = Vtiger_Record_Model::getInstanceById($recordId, $moduleName);
    $params=array('email_check' => 1, 'intapi'=>1, 'mode' => $mode);
    $params['user_email'] = $recordModel->get('email');
    if('setpass' == $mode){
      $params['pwd'] = $request->get('pass');
    }
    else{
      $myzoneid = $recordModel->get('myzoneid');
      if(!empty($myzoneid)){
        return array('id' => $myzoneid );
      }
    }

    $result = $this->doCall($params);

    if('testmail' == $mode && $result->id){
      $recordModel->set('myzoneid',$result->id);
      $recordModel->set('mode', 'edit');
      $recordModel->save();
    }
    return $result;
  }

  private function doCall($params){

    $httpClient = new Vtiger_Net_Client(MY_URL);
    $response = $httpClient->doPost($params);
    $response = trim($response);
    if ($response == "Error" || $response == "" || $response == null
            || $response == "Authentication Failure" ) {
            return false;
        }


    return json_decode($response);
  }
}
