<?php

class CustomLeads_MassSave_Action extends Vtiger_MassSave_Action {


  function getRecordModelsFromRequest(Vtiger_Request $request) {

    $moduleName = $request->getModule();
    $moduleModel = Vtiger_Module_Model::getInstance($moduleName);
    $recordIds = $this->getRecordsListFromRequest($request);
    $recordModels = array();

    $fieldModelList = $moduleModel->getFields();
    foreach($recordIds as $recId => $recordId) {
      $recordModel = Vtiger_Record_Model::getInstanceById($recordId, $moduleModel);
      $recordModel->set('id', $recordId);
      $recordModel->set('mode', 'edit');

      foreach ($fieldModelList as $fieldName => $fieldModel) {
        $fieldValue = $request->get($fieldName, null);
        $fieldDataType = $fieldModel->getFieldDataType();
        if($fieldDataType == 'time'){
          $fieldValue = Vtiger_Time_UIType::getTimeValueWithSeconds($fieldValue);
        }
        if($fieldName == 'assigned_user_id' && isset($fieldValue) && is_array($fieldValue)){
          $ass_id = $recId % count($fieldValue);
          $fieldValue = $fieldValue[$ass_id];
        }
        if(isset($fieldValue) && $fieldValue != null) {
          if(!is_array($fieldValue)) {
            $fieldValue = trim($fieldValue);
          }
          $recordModel->set($fieldName, $fieldValue);
        } else {
                    $uiType = $fieldModel->get('uitype');
                    if($uiType == 70) {
                        $recordModel->set($fieldName, $recordModel->get($fieldName));
                    }  else {
                        $uiTypeModel = $fieldModel->getUITypeModel();
                        $recordModel->set($fieldName, $uiTypeModel->getUserRequestValue($recordModel->get($fieldName)));
                    }
        }
      }
      $recordModels[$recordId] = $recordModel;
    }
    return $recordModels;
  }

} //End of Class
