<?php

require_once 'vtlib/Vtiger/Net/Client.php';
require_once 'include/Webservices/Utils.php';
define('MY_URL','http://t.seopex.net/api/conversions/update/');

function AffApi( $entry ){

  //return;
  global $current_user;
  $customleadid = vtws_getIdComponents($entry->getId())[1];
  //$email = $entry->get('email');
	$status = $entry->get('customleadstatus');
	$createdtime = $entry->get('createdtime');

	//error_log(var_export(array($customleadid,$status),true));
	$params=array(
		"id" => $customleadid,
		"status" => $status,
		"ctime" => $createdtime,
	);

	$ret = doCall($params);
	//error_log(var_export($ret,true));
}

function doCall($params){

		$url = MY_URL.$params["id"];
		//error_log($url);
    $httpClient = new Vtiger_Net_Client($url);
    $response = $httpClient->doPost($params);
    $response = trim($response);
    if ($response == "Error" || $response == "" || $response == null
            || $response == "Authentication Failure" ) {
            return false;
        }


    return $response;
    //return json_decode($response);
  }
