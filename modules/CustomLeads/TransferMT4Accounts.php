<?php

function transferMT4Accounts( $entry ){

  //return;
  global $current_user;
  $customleadid = $entry->getId();
  $mod = "MT4Accounts";
  $asigned = $entry->get('assigned_user_id');

  include_once 'include/Webservices/RetrieveRelated.php';
  include_once 'include/Webservices/Revise.php';
  $models = vtws_retrieve_related($customleadid,$mod,$mod,$current_user);

//var_dump($models);

  foreach($models as $model){
    $tmp_mod = array(
      "id" => $model["id"],
      "assigned_user_id" => $asigned,
    );
    vtws_revise($tmp_mod,$current_user);
  }

}
