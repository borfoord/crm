<?php

include_once 'modules/Vtiger/CRMEntity.php';

class SProcess extends Vtiger_CRMEntity {
        var $table_name = 'vtiger_sprocess';
        var $table_index= 'sprocessid';

        var $customFieldTable = Array('vtiger_sprocesscf', 'sprocessid');

        var $tab_name = Array('vtiger_crmentity', 'vtiger_sprocess', 'vtiger_sprocesscf');

        var $tab_name_index = Array(
                'vtiger_crmentity' => 'crmid',
                'vtiger_sprocess' => 'sprocessid',
                'vtiger_sprocesscf'=>'sprocessid');

        var $list_fields = Array (
                /* Format: Field Label => Array(tablename, columnname) */
                // tablename should not have prefix 'vtiger_'
                'SaleProcess' => Array('sprocess', 'sprocessid'),
                'Assigned To' => Array('crmentity','smownerid')
        );
        var $list_fields_name = Array (
                /* Format: Field Label => fieldname */
                'SaleProcess' => 'sprocess',
                'Assigned To' => 'assigned_user_id',
        );

        // Make the field link to detail view
        var $list_link_field = 'spname';

        // For Popup listview and UI type support
        var $search_fields = Array(
                /* Format: Field Label => Array(tablename, columnname) */
                // tablename should not have prefix 'vtiger_'
                'SaleProcess' => Array('sprocess', 'spname'),
                'Assigned To' => Array('vtiger_crmentity','assigned_user_id'),
        );
        var $search_fields_name = Array (
                /* Format: Field Label => fieldname */
                'SaleProcess' => 'spname',
                'Assigned To' => 'assigned_user_id',
        );

        // For Popup window record selection
        var $popup_fields = Array ('spname');

        // For Alphabetical search
        var $def_basicsearch_col = 'spname';

        // Column value to use on detail view record text display
        var $def_detailview_recname = 'spname';

        // Used when enabling/disabling the mandatory fields for the module.
        // Refers to vtiger_field.fieldname values.
        var $mandatory_fields = Array('spname','assigned_user_id');

        var $default_order_by = 'spname';
        var $default_sort_order='ASC';
}
