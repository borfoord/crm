<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/

class LeadsIB_Module_Model extends Vtiger_Module_Model {
	/**
	 * Function to get the Quick Links for the module
	 * @param <Array> $linkParams
	 * @return <Array> List of Vtiger_Link_Model instances
	 */


public function getLeadsBySSgb($owner,$dateFilter,$goodstatus,$ifGroupUse = 0) 
{
    		$db = PearDatabase::getInstance();
	
		$addIfGroupSql = 'and vtiger_usersLeads.last_name NOT LIKE "" AND vtiger_crmentity.crmid!="" ';
			if($ifGroupUse)
				$addIfGroupSql = '';

		
        if($owner!='')
		    $ownerSql = $this->getOwnerWhereConditionForDashBoards($owner);

		if(!empty($ownerSql)) {
			$ownerSql = ' AND '.$ownerSql;
		}
		
		$params = array();
		if(!empty($dateFilter)) {
			$dateFilterSql = ' AND createdtime BETWEEN ? AND ? ';
			//client is not giving time frame so we are appending it
			$params[] = $dateFilter['start']. ' 00:00:00';
			$params[] = $dateFilter['end']. ' 23:59:59';
		}

		
		$pQuery = 'SELECT COUNT(*) as count, 
		
		                CASE WHEN vtiger_leadstatus.leadstatus IS NULL OR vtiger_leadstatus.leadstatus = "" THEN "" ELSE 
						vtiger_leadstatus.leadstatus END AS leadstatusvalue,
						
						CASE WHEN vtiger_leaddetails.leadsource IS NULL OR vtiger_leaddetails.leadsource = "" THEN "" 
						ELSE vtiger_leaddetails.leadsource END AS leadsourcevalue
						
						FROM vtiger_leaddetails 
						INNER JOIN vtiger_crmentity ON vtiger_leaddetails.leadid = vtiger_crmentity.crmid
						AND deleted=0 AND converted = 0 '.  Users_Privileges_Model::getNonAdminAccessControlQuery('Leads'). $ownerSql .' '.$dateFilterSql.
						'LEFT JOIN vtiger_leadstatus ON vtiger_leaddetails.leadstatus = vtiger_leadstatus.leadstatus 
						
						LEFT JOIN vtiger_leadsource ON vtiger_leaddetails.leadsource = vtiger_leadsource.leadsource 
						
						GROUP BY leadstatusvalue,leadsourcevalue ORDER BY vtiger_leadstatus.sortorderid';
		
		
		
		
$CountGoodLeads = count($goodstatus);

if ($CountGoodLeads == 0)
    return;
else
{
$partQuery = 'CASE WHEN leadstatusvalue = "'.$goodstatus[0].'"';

for($i=1;$i < $CountGoodLeads;$i++)
    $partQuery = $partQuery. 'OR leadstatusvalue = "'.$goodstatus[$i].'" ';

$partQuery = $partQuery.'   then  "Good" else "Bad" end';


		$pQuery2 = 'select
		sum(count) count,
		'.$partQuery.' leadstatusvalue,
		leadsourcevalue
		from( '.$pQuery. ' ) sd
		group by '.$partQuery.'  ,
		leadsourcevalue
		';
		

		$result = $db->pquery($pQuery2, $params);

		for($i=0; $i<$db->num_rows($result); $i++) {
			$row = $db->query_result_rowdata($result, $i);
			$response[$i][0] = $row['count'];
			$leadStatusVal = $row['leadstatusvalue'];
			if($leadStatusVal == '') {
				$leadStatusVal = 'LBL_BLANK';
			}
			$response[$i][1] = vtranslate($leadStatusVal, $this->getName());

			$leadSourceVal =  $row['leadsourcevalue'];
			if($leadSourceVal == '') {
				$leadSourceVal = 'LBL_BLANK';
			}

			$response[$i][2] = vtranslate($leadSourceVal, $this->getName());
		}

		return $response;
}		
		
    
}


public function getLeadsBySA($owner,$dateFilter,$ifGroupUse = 0) 
{
	
		$db = PearDatabase::getInstance();
	
		$addIfGroupSql = 'and vtiger_usersLeads.last_name NOT LIKE "" AND vtiger_crmentity.crmid!="" ';
			if($ifGroupUse)
				$addIfGroupSql = '';

		
        if($owner!='')
		    $ownerSql = $this->getOwnerWhereConditionForDashBoards($owner);

		if(!empty($ownerSql)) {
			$ownerSql = ' AND '.$ownerSql;
		}
		
		$params = array();
		if(!empty($dateFilter)) {
			$dateFilterSql = ' AND createdtime BETWEEN ? AND ? ';
			//client is not giving time frame so we are appending it
			$params[] = $dateFilter['start']. ' 00:00:00';
			$params[] = $dateFilter['end']. ' 23:59:59';
		}

		$pQuery = 'SELECT COUNT(*) as count, 
		
		                CASE WHEN vtiger_leadstatus.leadstatus IS NULL OR vtiger_leadstatus.leadstatus = "" THEN "" ELSE 
						vtiger_leadstatus.leadstatus END AS leadstatusvalue,
                        tow.Leads_Assigned_To leadsourcevalue

						FROM vtiger_leaddetails 
						
						INNER JOIN vtiger_crmentity ON vtiger_leaddetails.leadid = vtiger_crmentity.crmid
						
						AND deleted=0 AND converted = 0 '.Users_Privileges_Model::getNonAdminAccessControlQuery("Leads"). $ownerSql .' '.$dateFilterSql.
						
						'
						LEFT JOIN vtiger_leadstatus ON vtiger_leaddetails.leadstatus = vtiger_leadstatus.leadstatus 
						
						LEFT JOIN (
						
						SELECT
  vtiger_leaddetails.`leadid` ,
  CASE
  WHEN(vtiger_usersLeads.last_name NOT LIKE "" AND vtiger_crmentity.crmid!="" )
  THEN CONCAT(vtiger_usersLeads.first_name," ",vtiger_usersLeads.last_name)
  ELSE vtiger_groupsLeads.groupname
  END
    AS "Leads_Assigned_To",
	
CASE	
	 WHEN(vtiger_usersLeads.last_name NOT LIKE "" AND vtiger_crmentity.crmid!="" )
  THEN 1
  ELSE 2
  END
    AS "type_Leads_Assigned_To",
	
    vtiger_crmentity.smownerid AS "Owner"
    
  FROM vtiger_leaddetails
  INNER JOIN vtiger_crmentity
    ON vtiger_crmentity.crmid=vtiger_leaddetails.leadid
  LEFT JOIN vtiger_groups AS vtiger_groupsLeads
    ON vtiger_groupsLeads.groupid = vtiger_crmentity.smownerid
  LEFT JOIN vtiger_users AS vtiger_usersLeads
    ON vtiger_usersLeads.id = vtiger_crmentity.smownerid
  LEFT JOIN vtiger_groups
    ON vtiger_groups.groupid = vtiger_crmentity.smownerid
  LEFT join vtiger_users
    ON vtiger_users.id = vtiger_crmentity.smownerid
  WHERE vtiger_leaddetails.leadid > 0 '.$addIfGroupSql.'
    AND vtiger_crmentity.deleted=0 AND vtiger_leaddetails.converted=0
						) tow ON  tow.leadid = vtiger_leaddetails.leadid
						GROUP BY leadstatusvalue,  Leads_Assigned_To  ORDER BY vtiger_leadstatus.sortorderid';
		
		$result = $db->pquery($pQuery, $params);

		$response = array();
		
		for($i=0; $i<$db->num_rows($result); $i++) {
			$row = $db->query_result_rowdata($result, $i);
			$response[$i][0] = $row['count'];
			$leadStatusVal = $row['leadstatusvalue'];
			if($leadStatusVal == '') {
				$leadStatusVal = 'LBL_BLANK';
			}
			$response[$i][1] = vtranslate($leadStatusVal, $this->getName());

			$leadSourceVal =  $row['leadsourcevalue'];
			if($leadSourceVal == '') {
				$leadSourceVal = 'Other';
			}

			$response[$i][2] = vtranslate($leadSourceVal, $this->getName());
		}


		return $response;
	}
	 



public function getLeadsBySAgb($owner,$dateFilter,$goodstatus,$ifGroupUse = 0) 
{
    
		$db = PearDatabase::getInstance();
	
		$addIfGroupSql = 'and vtiger_usersLeads.last_name NOT LIKE "" AND vtiger_crmentity.crmid!="" ';
			if($ifGroupUse)
				$addIfGroupSql = '';

		
        if($owner!='')
		    $ownerSql = $this->getOwnerWhereConditionForDashBoards($owner);

		if(!empty($ownerSql)) {
			$ownerSql = ' AND '.$ownerSql;
		}
		
		$params = array();
		if(!empty($dateFilter)) {
			$dateFilterSql = ' AND createdtime BETWEEN ? AND ? ';
			//client is not giving time frame so we are appending it
			$params[] = $dateFilter['start']. ' 00:00:00';
			$params[] = $dateFilter['end']. ' 23:59:59';
		}

		$pQuery = 'SELECT COUNT(*) as count, 
		
		                CASE WHEN vtiger_leadstatus.leadstatus IS NULL OR vtiger_leadstatus.leadstatus = "" THEN "" ELSE 
						vtiger_leadstatus.leadstatus END AS leadstatusvalue,
                        tow.Leads_Assigned_To leadsourcevalue

						FROM vtiger_leaddetails 
						
						INNER JOIN vtiger_crmentity ON vtiger_leaddetails.leadid = vtiger_crmentity.crmid
						
						AND deleted=0 AND converted = 0 '.Users_Privileges_Model::getNonAdminAccessControlQuery("Leads"). $ownerSql .' '.$dateFilterSql.
						
						'
						LEFT JOIN vtiger_leadstatus ON vtiger_leaddetails.leadstatus = vtiger_leadstatus.leadstatus 
						
						LEFT JOIN (
						
						SELECT
  vtiger_leaddetails.`leadid` ,
  CASE
  WHEN(vtiger_usersLeads.last_name NOT LIKE "" AND vtiger_crmentity.crmid!="" )
  THEN CONCAT(vtiger_usersLeads.first_name," ",vtiger_usersLeads.last_name)
  ELSE vtiger_groupsLeads.groupname
  END
    AS "Leads_Assigned_To",
	
CASE	
	 WHEN(vtiger_usersLeads.last_name NOT LIKE "" AND vtiger_crmentity.crmid!="" )
  THEN 1
  ELSE 2
  END
    AS "type_Leads_Assigned_To",
	
    vtiger_crmentity.smownerid AS "Owner"
    
  FROM vtiger_leaddetails
  INNER JOIN vtiger_crmentity
    ON vtiger_crmentity.crmid=vtiger_leaddetails.leadid
  LEFT JOIN vtiger_groups AS vtiger_groupsLeads
    ON vtiger_groupsLeads.groupid = vtiger_crmentity.smownerid
  LEFT JOIN vtiger_users AS vtiger_usersLeads
    ON vtiger_usersLeads.id = vtiger_crmentity.smownerid
  LEFT JOIN vtiger_groups
    ON vtiger_groups.groupid = vtiger_crmentity.smownerid
  LEFT join vtiger_users
    ON vtiger_users.id = vtiger_crmentity.smownerid
  WHERE vtiger_leaddetails.leadid > 0 '.$addIfGroupSql.'
    AND vtiger_crmentity.deleted=0 AND vtiger_leaddetails.converted=0
						) tow ON  tow.leadid = vtiger_leaddetails.leadid
						GROUP BY leadstatusvalue,  Leads_Assigned_To  ORDER BY vtiger_leadstatus.sortorderid';
		



$CountGoodLeads = count($goodstatus);

if ($CountGoodLeads == 0)
    return;
else
{
$partQuery = 'CASE WHEN leadstatusvalue = "'.$goodstatus[0].'"';

for($i=1;$i < $CountGoodLeads;$i++)
    $partQuery = $partQuery. 'OR leadstatusvalue = "'.$goodstatus[$i].'" ';

$partQuery = $partQuery.'   then  "Good" else "Bad" end';


		$pQuery2 = 'select
		sum(count) count,
		'.$partQuery.' leadstatusvalue,
		leadsourcevalue
		from( '.$pQuery. ' ) sd
		group by '.$partQuery.'  ,
		leadsourcevalue
		';
		

		$result = $db->pquery($pQuery2, $params);

		for($i=0; $i<$db->num_rows($result); $i++) {
			$row = $db->query_result_rowdata($result, $i);
			$response[$i][0] = $row['count'];
			$leadStatusVal = $row['leadstatusvalue'];
			if($leadStatusVal == '') {
				$leadStatusVal = 'LBL_BLANK';
			}
			$response[$i][1] = vtranslate($leadStatusVal, $this->getName());

			$leadSourceVal =  $row['leadsourcevalue'];
			if($leadSourceVal == '') {
				$leadSourceVal = 'Other';
			}

			$response[$i][2] = vtranslate($leadSourceVal, $this->getName());
		}

		return $response;
}

}






	 
	 
public function getLeadsByStatusAndAssignedBy($owner,$dateFilter) {
		$db = PearDatabase::getInstance();

		$ownerSql = $this->getOwnerWhereConditionForDashBoards($owner);
		if(!empty($ownerSql)) {
			$ownerSql = ' AND '.$ownerSql;
		}
		
		$params = array();
		if(!empty($dateFilter)) {
			$dateFilterSql = ' AND createdtime BETWEEN ? AND ? ';
			//client is not giving time frame so we are appending it
			$params[] = $dateFilter['start']. ' 00:00:00';
			$params[] = $dateFilter['end']. ' 23:59:59';
		}

		$result = $db->pquery('SELECT COUNT(*) as count, 
		
		                CASE WHEN vtiger_leadstatus.leadstatus IS NULL OR vtiger_leadstatus.leadstatus = "" THEN "" ELSE 
						vtiger_leadstatus.leadstatus END AS leadstatusvalue,
                        tow.Leads_Assigned_To leadsourcevalue

						FROM vtiger_leaddetails 
						
						INNER JOIN vtiger_crmentity ON vtiger_leaddetails.leadid = vtiger_crmentity.crmid
						
						AND deleted=0 AND converted = 0 '.Users_Privileges_Model::getNonAdminAccessControlQuery($this->getName()). $ownerSql .' '.$dateFilterSql.
						
						'INNER JOIN vtiger_leadstatus ON vtiger_leaddetails.leadstatus = vtiger_leadstatus.leadstatus 
						
						INNER JOIN (
						
						SELECT
  vtiger_leaddetails.`leadid` ,
  CASE
  WHEN(vtiger_usersLeads.last_name NOT LIKE "" AND vtiger_crmentity.crmid!="" )
  THEN CONCAT(vtiger_usersLeads.first_name," ",vtiger_usersLeads.last_name)
  ELSE vtiger_groupsLeads.groupname
  END
    AS "Leads_Assigned_To",
    vtiger_crmentity.smownerid AS "Owner"
    
  FROM vtiger_leaddetails
  INNER JOIN vtiger_crmentity
    ON vtiger_crmentity.crmid=vtiger_leaddetails.leadid
  LEFT JOIN vtiger_groups AS vtiger_groupsLeads
    ON vtiger_groupsLeads.groupid = vtiger_crmentity.smownerid
  LEFT JOIN vtiger_users AS vtiger_usersLeads
    ON vtiger_usersLeads.id = vtiger_crmentity.smownerid
  LEFT JOIN vtiger_groups
    ON vtiger_groups.groupid = vtiger_crmentity.smownerid
  LEFT join vtiger_users
    ON vtiger_users.id = vtiger_crmentity.smownerid
  WHERE vtiger_leaddetails.leadid > 0
    AND vtiger_crmentity.deleted=0 AND vtiger_leaddetails.converted=0
    AND (( TRIM(CASE WHEN (vtiger_usersLeads.last_name NOT LIKE "")
      THEN CONCAT(vtiger_usersLeads.first_name," ",vtiger_usersLeads.last_name)
      ELSE vtiger_groupsLeads.groupname END) <> "Administrator"
       ) ) 

	   ) tow ON  tow.leadid = vtiger_leaddetails.leadid

		GROUP BY leadstatusvalue,  Leads_Assigned_To  ORDER BY vtiger_leadstatus.sortorderid', $params);

		$response = array();
	

		
		for($i=0; $i<$db->num_rows($result); $i++) {
			$row = $db->query_result_rowdata($result, $i);
			$response[$i][0] = $row['count'];
			$leadStatusVal = $row['leadstatusvalue'];
			if($leadStatusVal == '') {
				$leadStatusVal = 'LBL_BLANK';
			}
			$response[$i][1] = vtranslate($leadStatusVal, $this->getName());

			$leadSourceVal =  $row['leadsourcevalue'];
			if($leadSourceVal == '') {
				$leadSourceVal = 'LBL_BLANK';
			}

			$response[$i][2] = vtranslate($leadSourceVal, $this->getName());
		}
		return $response;
	}	 
	
	
	
	
	
	 
	 
public function getLeadsBySS($owner,$dateFilter) {
		$db = PearDatabase::getInstance();

        if($owner!='')
		    $ownerSql = $this->getOwnerWhereConditionForDashBoards($owner);

		if(!empty($ownerSql)) {
			$ownerSql = ' AND '.$ownerSql;
		}
		
		$params = array();
		if(!empty($dateFilter)) {
			$dateFilterSql = ' AND createdtime BETWEEN ? AND ? ';
			//client is not giving time frame so we are appending it
			$params[] = $dateFilter['start']. ' 00:00:00';
			$params[] = $dateFilter['end']. ' 23:59:59';
		}

		$result = $db->pquery('SELECT COUNT(*) as count, 
		
		                CASE WHEN vtiger_leadstatus.leadstatus IS NULL OR vtiger_leadstatus.leadstatus = "" THEN "" ELSE 
						vtiger_leadstatus.leadstatus END AS leadstatusvalue,
						
						CASE WHEN vtiger_leaddetails.leadsource IS NULL OR vtiger_leaddetails.leadsource = "" THEN "" 
						ELSE vtiger_leaddetails.leadsource END AS leadsourcevalue
						
						FROM vtiger_leaddetails 
						INNER JOIN vtiger_crmentity ON vtiger_leaddetails.leadid = vtiger_crmentity.crmid
						AND deleted=0 AND converted = 0 '.  Users_Privileges_Model::getNonAdminAccessControlQuery('Leads'). $ownerSql .' '.$dateFilterSql.
						'LEFT JOIN vtiger_leadstatus ON vtiger_leaddetails.leadstatus = vtiger_leadstatus.leadstatus 
						
						LEFT JOIN vtiger_leadsource ON vtiger_leaddetails.leadsource = vtiger_leadsource.leadsource 
						
						GROUP BY leadstatusvalue,leadsourcevalue ORDER BY vtiger_leadstatus.sortorderid', $params);

		$response = array();
		
		
		
		for($i=0; $i<$db->num_rows($result); $i++) {
			$row = $db->query_result_rowdata($result, $i);
			$response[$i][0] = $row['count'];
			$leadStatusVal = $row['leadstatusvalue'];
			if($leadStatusVal == '') {
				$leadStatusVal = 'LBL_BLANK';
			}
			$response[$i][1] = vtranslate($leadStatusVal, $this->getName());

			$leadSourceVal =  $row['leadsourcevalue'];
			if($leadSourceVal == '') {
				$leadSourceVal = 'LBL_BLANK';
			}

			$response[$i][2] = vtranslate($leadSourceVal, $this->getName());
		}
		return $response;
	}
	 
	 
	
}