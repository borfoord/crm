<?php

// include_once 'modules/Leads/Leads.php';
// extends Leads

class LeadsIB  {

	function LeadsIB()	{
		$this->log = LoggerManager::getLogger('LeadsIB ');
		$this->log->debug("Entering LeadsIB() method ...");
		$this->db = PearDatabase::getInstance();
		$this->column_fields = getColumnFields('Leads');
		$this->log->debug("Exiting LeadsIB method ...");
	}	
	
        function vtlib_handler($moduleName, $eventType) 
		{

			if ($eventType == 'module.postinstall') {
					
								include_once('vtlib/Vtiger/Module.php');
								$moduleInstance = Vtiger_Module::getInstance('Home');
								$moduleInstance->addLink('DASHBOARDWIDGET', 'Leads by Status & Source Ext', 'index.php?module=LeadsIB&view=ShowWidget&name=LeadsBySS','', '10');
								$moduleInstance->addLink('DASHBOARDWIDGET', 'Leads by Status & Assigned Ext', 'index.php?module=LeadsIB&view=ShowWidget&name=LeadsBySA','', '11');
								$moduleInstance->setDefaultSharing();
                                // LeadsIB::create($data);
								
								$moduleInstance->addLink('HEADERSCRIPT', 'ajax-last-visit-piwik ', 'get.js');
								
                }
			if ($eventType == 'module.postupdate') {	
								
								include_once('vtlib/Vtiger/Module.php');
								$moduleInstance = Vtiger_Module::getInstance('Home');
								$moduleInstance->addLink('DASHBOARDWIDGET', 'Leads by Status & Source Ext', 'index.php?module=LeadsIB&view=ShowWidget&name=LeadsBySS','', '10');
								$moduleInstance->addLink('DASHBOARDWIDGET', 'Leads by Status & Assigned Ext', 'index.php?module=LeadsIB&view=ShowWidget&name=LeadsBySA','', '11');
								$moduleInstance->setDefaultSharing();		
								
								$moduleInstance->addLink('HEADERSCRIPT', 'ajax-last-visit-piwik ', 'get.js');
			}
        }
        
        
        
	
}