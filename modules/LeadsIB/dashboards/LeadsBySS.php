<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/

class LeadsIB_LeadsBySS_Dashboard extends Vtiger_IndexAjax_View {

    var $widgetid;	
    
    function getSearchParams($valueStatus,$valueSource,$assignedto,$dates) {
        
        $listSearchParams = array();
        $conditions = array();

        if ($valueSource != '-Blank-' and $valueSource != '')
            array_push($conditions,array('leadsource','e',$valueSource));
        else
            if($valueSource == '-Blank-') 
                array_push($conditions,array('leadsource','y'));
            

        if ($valueStatus != '-Blank-' and $valueStatus != '' )
            array_push($conditions,array('leadstatus','e',$valueStatus));
        else
            if($valueStatus == '-Blank-') 
                array_push($conditions,array('leadstatus','y'));


        if($assignedto != '') array_push($conditions,array('assigned_user_id','e',getUserFullName($assignedto)));
        
        if(!empty($dates)){
            array_push($conditions,array('createdtime','bw',$dates['start'].' 00:00:00,'.$dates['end'].' 23:59:59'));
        }
        
        $listSearchParams[] = $conditions;
        
        return '&search_params='.urlencode(json_encode($listSearchParams));
    }


	public function UpdateUserWidgetParam($param_arr) {

		$currentUser = Users_Record_Model::getCurrentUserModel();
		$wdata = html_entity_decode($currentUser->get('wdata'));
		
		$arrWithJSON =  json_decode($wdata,true); 
		$arrWithJSON[$this->widgetid] = $param_arr;	 

        $currentUser->set('wdata',json_encode($arrWithJSON));
        $currentUser->set('mode','edit');
        $currentUser->save();        	
	}


	public function GetUserWidgetParam() {
	    $currentUser = Users_Record_Model::getCurrentUserModel();
	    $wdata = html_entity_decode($currentUser->get('wdata'));
	    $arrWithJSON =  json_decode($wdata,true); 
	    return $arrWithJSON[$this->widgetid]; 
	}



	public function process(Vtiger_Request $request) {
	    
		$currentUser = Users_Record_Model::getCurrentUserModel();
		$viewer = $this->getViewer($request);
		$moduleName = $request->getModule();

		$linkId = $request->get('linkid');
		$data = $request->get('data');
		$createdTime = $request->get('createdtime');
		$content = $request->get('content');
		$goodStatusArray = $request->get('goodleadstatus');
		
		$DataSettings = $request->get('datasettings');
		
		$widget = Vtiger_Widget_Model::getInstance($linkId, $currentUser->getId());
		$this->widgetid = $widget->get('id');

		
		
		$wdata = $currentUser->get('wdata');
			if( !empty($content)  ) 
			{
			    $paramarr = array();
				if (isset($createdTime['start']) )
				{
					$paramarr['dt']= $createdTime;
					$dates['start'] = Vtiger_Date_UIType::getDBInsertedValue($createdTime['start']);
					$dates['end'] = Vtiger_Date_UIType::getDBInsertedValue($createdTime['end']);
					$dateCaption = 'from '. $createdTime['start'].' to '.$createdTime['end'];
					
				}

				$smownerid = $request->get('smownerid');
				$paramarr['oid']=$smownerid;
				$paramarr['ds']=$DataSettings;
				$paramarr['gs'] = $goodStatusArray;
				if(is_string($wdata))
				{
					$this->UpdateUserWidgetParam($paramarr);
				}					
			}
			else {
				$arrWithParam = $this->GetUserWidgetParam();
    				if ( !is_null($arrWithParam) )
    				{
                        if(isset($arrWithParam['dt']))
                        {
    					    $createdTime['start'] = $arrWithParam['dt']['start'];
    					    $createdTime['end'] = $arrWithParam['dt']['end'];
    					    $dates['start'] = Vtiger_Date_UIType::getDBInsertedValue($createdTime['start']);
    					    $dates['end'] = Vtiger_Date_UIType::getDBInsertedValue($createdTime['end']);		    
    					    $dateOut='value="'.$createdTime['start'].','.$createdTime['end'].'"';
							$dateCaption = 'from '. $createdTime['start'].' to '.$createdTime['end'];
                        }
    					$smownerid = $arrWithParam['oid'];
						$DataSettings = $arrWithParam['ds'];
						$goodStatusArray = $arrWithParam['gs'];
    				}    		    
				}



		$moduleModelLeads = Vtiger_Module_Model::getInstance('Leads');
		$listViewUrl = $moduleModelLeads->getListViewUrl();
		
        if ($moduleModelLeads->isActive()) 
        {
  			$arrFiels = $moduleModelLeads->getFields();
  			$fieldSource = $arrFiels['leadstatus'];
  			$fieldStatus = $arrFiels['leadsource'];
        }	

        $moduleModel = Vtiger_Module_Model::getInstance($moduleName);
		$data = $moduleModel->getLeadsBySS($smownerid,$dates);

        
        $arrX = array();
        $arrY = array();
        
        $MaxValue = 0;
        
        for($i = 0;$i<count($data);$i++){
        
            if (  !in_array ( $data[$i][1] ,$arrX  )  )        
                $arrX[] = $data[$i][1];

            if (  !in_array ( $data[$i][2] ,$arrY  )  )        
                $arrY[] = $data[$i][2];


            if ($data[$i][0] > $MaxValue)
                $MaxValue = $data[$i][0];

            $arrSumY[$data[$i][1]]['value'] = $arrSumY[$data[$i][1]]['value'] + $data[$i][0];
            $arrSumX[$data[$i][2]]['value'] = $arrSumX[$data[$i][2]]['value'] + $data[$i][0];
            

            $arrSumY[$data[$i][1]]['links'] = $listViewUrl.$this->getSearchParams($data[$i][1],"",$smownerid,$dates);
            $arrSumX[$data[$i][2]]['links'] = $listViewUrl.$this->getSearchParams("",$data[$i][2],$smownerid,$dates);

            $allTotal =  $allTotal  + $data[$i][0];

            $outArray[ $data[$i][1]][$data[$i][2]]['value'] = $data[$i][0];

            $outArray[ $data[$i][1]][$data[$i][2]]['links']  =   $listViewUrl.$this->getSearchParams($data[$i][1],$data[$i][2],$smownerid,$dates) ;
        }

        for($i = 0;$i<count($data);$i++){
            
            $color = 255 - ($data[$i][0] * 255) / $MaxValue ;
            if($color < 16)
                $color = '0'.  dechex($color ) ;
            else                 
                $color = dechex($color ) ;
                
            $outArray[ $data[$i][1]][$data[$i][2]]['style'] =  'style="text-align: center;background-color: #'.$color.'FFFF;" '; 
        }


		$data2 = $moduleModel->getLeadsBySSgb($smownerid,$dates,$goodStatusArray,$ifGroupUse);

        $AllgoodLeads = 0;
        $AllbadLeads =0;
        for($i = 0;$i<count ($data2);$i++)
        {
            if($data2[$i][1] == "Good")
            {
                $goodLeads[$data2[$i][2]] = $data2[$i][0];
                $AllgoodLeads = $AllgoodLeads + $data2[$i][0];
            }    
            
            if($data2[$i][1] == "Bad")
            {
                $badLeads[$data2[$i][2]] = $data2[$i][0]; 
                $AllbadLeads = $AllbadLeads + $data2[$i][0];
            }    
        }        

		
		$viewer->assign('ALLBADLEADS', $AllbadLeads);
		$viewer->assign('ALLGOODLEADS', $AllgoodLeads);
		
		
		$viewer->assign('GOODARRAY', $goodLeads);
		$viewer->assign('BADARRAY', $badLeads);


		$widget = Vtiger_Widget_Model::getInstance($linkId, $currentUser->getId());

		//Include special script and css needed for this widget
		$viewer->assign('HEADER_WIDGET_ID', 'idwidget_'.$widget->getName());
		$viewer->assign('HEADER_WIDGET_ID_WITH_DATA', 'iddatacaption_'.$widget->getName()); 

		
		$viewer->assign('WIDGET', $widget);
		$viewer->assign('MODULE_NAME', $moduleName);
		$viewer->assign('DATA', $data);

        $viewer->assign('OUTARRAY', $outArray);


		switch ($DataSettings) {
			case 0:
					$varSetX = $fieldSource->getpicklistvalues();
					$varSetY = $fieldStatus->getpicklistvalues();
					$varSetX[] = vtranslate('LBL_BLANK' );
					$varSetY[] = vtranslate('LBL_BLANK' );
				break;
			case 1:
					$varSetX = $fieldSource->getpicklistvalues();
					$varSetX[] = vtranslate('LBL_BLANK' );
					$varSetY = $arrY;
				break;
			case 2:
					$varSetY = $fieldStatus->getpicklistvalues();
					$varSetY[] = vtranslate('LBL_BLANK' );
					$varSetX = $arrX;
				break;
			case 3:
					$varSetX = $arrX;
					$varSetY = $arrY;
				break;
			DEFAULT:
					$varSetX = $fieldSource->getpicklistvalues();
					$varSetY = $fieldStatus->getpicklistvalues();
					$varSetX[] = vtranslate('LBL_BLANK' );
					$varSetY[] = vtranslate('LBL_BLANK' );
			break;					
		}		


		$viewer->assign('IFGROUPUSE',$ifGroupUse);
			
		if ($ifGroupUse)
		{
			//$accessibleUsers = $currentUser->getAccessibleUsersForModule('Leads');
			$viewer->assign('ARRWITHUSERS',$currentUser->getAccessibleUsersForModule('Leads') );
			$viewer->assign('ARRWITHGROUP',$currentUser->getAccessibleGroupForModule('Leads') );
			$viewer->assign('ACCESSIBLE_USERS',$ValueForX );
		}
		else
			$viewer->assign('ACCESSIBLE_USERS',$currentUser->getAccessibleUsersForModule('Leads'));


		$viewer->assign('SELECT2STATUSVALUE', $fieldSource->getpicklistvalues()); 
		$viewer->assign('SELECT2STATUSVALUESELECTED',$goodStatusArray ); 
		
		
		//print_r($goodStatusArray);

		
		$viewer->assign('DATASETTINGS', $DataSettings);
		
        $viewer->assign('ARRX', $varSetX);
        $viewer->assign('ARRY', $varSetY);


        $viewer->assign('ARRSUMX', $arrSumX);
        $viewer->assign('ARRSUMY', $arrSumY);
        $viewer->assign('ALLTOTAL', $allTotal);


		$viewer->assign('SMOWNERID', $smownerid);

		$viewer->assign('DATECAPTION', $dateCaption);
		$viewer->assign('DATEOUT', $dateOut);
		$viewer->assign('CURRENTUSER', $currentUser);

		
		$viewer->assign('YWIDZET', floor((35 * count($varSetY) + 150)/100)  + 1);
		$viewer->assign('XWIDZET', floor((70 * count($varSetX) + 300)/100)  + 1);
		
		
		$accessibleUsers = $currentUser->getAccessibleUsersForModule('Leads');
		$viewer->assign('ACCESSIBLE_USERS', $accessibleUsers);
		
		$content = $request->get('content');
		
		if(!empty($content)) {
			$viewer->view('dashboards/LeadsBySSContents.tpl', $moduleName);
//			$viewer->view('dashboards/LeadsBySource.tpl', $moduleName);
		} else {
            //$viewer->view('dashboards/DashBoardWidgetContents.tpl', $moduleName);
			$viewer->view('dashboards/LeadsBySS.tpl', $moduleName);
		}
	}
}
