<?php


include_once 'modules/Vtiger/CRMEntity.php';

class IBService extends Vtiger_CRMEntity {
        var $table_name = 'vtiger_ibservice';
        var $table_index= 'ibserviceid';

        var $customFieldTable = Array('vtiger_ibservice', 'ibserviceid');

        var $tab_name = Array('vtiger_crmentity', 'vtiger_ibservice', 'vtiger_ibservicecf');

        var $tab_name_index = Array(
                'vtiger_crmentity' => 'crmid',
                'vtiger_ibservice' => 'ibserviceid',
                'vtiger_ibservicecf'=>'ibserviceid');

        var $list_fields = Array (
                /* Format: Field Label => Array(tablename, columnname) */
                // tablename should not have prefix 'vtiger_'
                'Service' => Array('ibservice', 'ibservice'),
                'Assigned To' => Array('crmentity','smownerid')
        );
        var $list_fields_name = Array (
                /* Format: Field Label => fieldname */
                'Service' => 'ibservice',
                'Assigned To' => 'assigned_user_id',
        );

        // Make the field link to detail view
        var $list_link_field = 'ibservice';

        // For Popup listview and UI type support
        var $search_fields = Array(
                /* Format: Field Label => Array(tablename, columnname) */
                // tablename should not have prefix 'vtiger_'
                'Service' => Array('ibservice', 'ibservice'),
                'Assigned To' => Array('vtiger_crmentity','assigned_user_id'),
        );
        var $search_fields_name = Array (
                /* Format: Field Label => fieldname */
                'Service' => 'ibservice',
                'Assigned To' => 'assigned_user_id',
        );

        // For Popup window record selection
        var $popup_fields = Array ('ibservice');

        // For Alphabetical search
        var $def_basicsearch_col = 'ibservice';

        // Column value to use on detail view record text display
        var $def_detailview_recname = 'ibservice';

        // Used when enabling/disabling the mandatory fields for the module.
        // Refers to vtiger_field.fieldname values.
        var $mandatory_fields = Array('ibservice','assigned_user_id');

        var $default_order_by = 'ibservice';
        var $default_sort_order='ASC';
}
