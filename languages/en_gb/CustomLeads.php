<?php
/*+**********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 ************************************************************************************/
$languageStrings = array(
	'CustomLeads'                        => 'CustomLeads'                       , 
	'SINGLE_CustomLeads'                 => 'CustomLead'                        , 
	'LBL_RECORDS_LIST'             => 'CustomLeads List'                  , 
	'LBL_ADD_RECORD'               => 'Add CustomLead'                    , 
	'LBL_CUSTOMLEAD_INFORMATION'         => 'CustomLead Information'            , 
	'CustomLead No'                      => 'CustomLead No.'                    , 
	'Company'                      => 'Company'                     , 
	'Designation'                  => 'Title'                       , 
	'Website'                      => 'Website'                     , 
	'Industry'                     => 'Industry'                    , 
	'CustomLead Status'                  => 'CustomLead Status'                 , 
	'No Of Employees'              => 'No. of Employees'             , 
	'--None--'                     => '--None--'                    , 
	'Mr.'                          => 'Mr.'                         , 
	'Ms.'                          => 'Ms.'                         , 
	'Mrs.'                         => 'Mrs.'                        , 
	'Dr.'                          => 'Dr.'                         , 
	'Prof.'                        => 'Prof.'                       , 
	'Attempted to Contact'         => 'Attempted to contact'        , 
	'Cold'                         => 'Cold'                        , 
	'Contact in Future'            => 'Contact in future'           , 
	'Contacted'                    => 'Contacted'                   , 
	'Hot'                          => 'Hot'                         , 
	'Junk CustomLead'                    => 'Junk CustomLead'                   , 
	'Lost CustomLead'                    => 'Lost CustomLead'                   , 
	'Not Contacted'                => 'Not Contacted'               , 
	'Pre Qualified'                => 'Pre Qualified'               , 
	'Qualified'                    => 'Qualified'                   , 
	'Warm'                         => 'Warm'                        , 
	'LBL_CONVERT_CUSTOMLEAD'             => 'Convert CustomLead:'               , 
	'LBL_TRANSFER_RELATED_RECORD'  => 'Transfer related record to'  , 
	'LBL_CONVERT_CUSTOMLEAD_ERROR'       => 'You have to enable either Organisation or Contact to convert the CustomLead', 
	'LBL_CONVERT_CUSTOMLEAD_ERROR_TITLE' => 'Modules Disabled'            , 
	'CANNOT_CONVERT'               => 'Cannot be converted'         , 
	'LBL_FOLLOWING_ARE_POSSIBLE_REASONS' => 'Following could be one of the possible reasons', 
	'LBL_CUSTOMLEADS_FIELD_MAPPING_INCOMPLETE' => 'All the mandatory fields are not mapped', 
	'LBL_MANDATORY_FIELDS_ARE_EMPTY' => 'Some of the mandatory field values are empty', 
	'LBL_CUSTOMLEADS_FIELD_MAPPING'      => 'CustomLeads Custom Field Mapping'  , 
	'LBL_CUSTOM_FIELD_MAPPING'     => 'Edit Field Mapping'          , 
	
);
$jsLanguageStrings = array(
	'JS_SELECT_CONTACTS'           => 'Select Contacts to proceed'  , 
	'JS_SELECT_ORGANIZATION'       => 'Select Organisation to proceed', 
	'JS_SELECT_ORGANIZATION_OR_CONTACT_TO_CONVERT_CUSTOMLEAD' => 'Conversion requires selection of Contact or Organisation', 
);