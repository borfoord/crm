<?php
/*+**********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved. 
 *  Author       : Francisco Hernandez Odin Consultores S de RL de CV
 *  Author       : www.odin.mx
 *  Author       : Proudly, the best Vtiger's Partner in Mexico
 ************************************************************************************/
$languageStrings = Array(
	'DragNDrop' => 'Drag N Drop',
	'SINGLE_DragNDrop' => 'Drag N Drop',
	'DragNDrop ID' => 'ID Drag N Drop',
	'LBL_CUSTOM_INFORMATION' => 'Información Personalizada',
	'LBL_DRAGNDROP_INFORMATION' => 'Historial de Drag N Drop',
	'LBL_DDD_TITLE' => 'Drag N Drop',
        'LBL_DDD_SETTINGS' => 'Configuración Drag N Drop',
        'LBL_STATUS' => 'Estado',
        'LBL_SETTINGS' => 'Configuración',
	'LBL_DRAGNDROP_SETTINGS' => 'Drag N Drop Config.',
	'LBL_DRAGNDROP_LOGS' => 'Historial Drag N Drop',
        'LBL_LIST_MODULES' => 'Módulos relacionados a Documentos',
);

?>
