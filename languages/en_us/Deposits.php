<?php

$languageStrings = array(
  'LBL_DEPOSITS_INFORMATION' => 'Account Information',
  'SINGLE_Deposits' => 'Deposit',
  'LBL_DEPOSITS_ZPINFO' => 'Deposit Information',
  'Deposits' => 'Financial Transactions',
  'card_type' => 'Payment Type',
  'depositStatus' => 'Status',
);
