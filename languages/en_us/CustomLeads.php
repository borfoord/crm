<?php
/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/
$languageStrings = array(
	'Attempted to Contact'=>'Attempted to Contact',
	'CANNOT_CONVERT' => 'Cannot Convert',
	'Cold'=>'Cold',
	'Company' => 'Company',
	'Contacted'=>'Contacted',
	'Contact in Future'=>'Contact in Future',
	'Designation' => 'Designation',
	'Dr.'=>'Dr.',
	'Email' => 'Primary Email',
	'Hot'=>'Hot',
	'Industry' => 'Industry',
	'Junk CustomLead'=>'Junk CustomLead',
	'LBL_ADD_RECORD' => 'Add CustomLead',
	'LBL_CONVERT_CUSTOMLEAD' => 'Convert CustomLead',
	'LBL_CONVERT_CUSTOMLEAD_ERROR_TITLE' => 'Modules Disabled',
	'LBL_CONVERT_CUSTOMLEAD_ERROR' => 'You have to enable either Organization or Contact to convert the CustomLead',
	'LBL_CUSTOM_FIELD_MAPPING'=> 'Edit Field Mapping',
	'LBL_FOLLOWING_ARE_POSSIBLE_REASONS' => 'Possible reasons include:',
	'LBL_CUSTOMLEAD_INFORMATION' => 'CustomLead Details',
	'LBL_CUSTOMLEADS_FIELD_MAPPING_INCOMPLETE' => 'CustomLeads Field Mapping is incomplete(Settings > Module Manager > CustomLeads > CustomLeads Field Mapping)',
	'LBL_CUSTOMLEADS_FIELD_MAPPING' => 'CustomLeads Field Mapping',
	'LBL_MANDATORY_FIELDS_ARE_EMPTY' => 'Mandatory fields are empty',
	'LBL_RECORDS_LIST' => 'CustomLeads List',
	'LBL_TRANSFER_RELATED_RECORD' => 'Transfer related record to',
	'CustomLead No' => 'CustomLead Number',
	'CustomLeads' => 'CustomLeads',
	'CustomLead Status' => 'CustomLead Status',
	'Lost CustomLead'=>'Lost CustomLead',
	'Mr.'=>'Mr.',
	'Mrs.'=>'Mrs.',
	'Ms.'=>'Ms.',
	'--None--'=>'--None--',
	'No Of Employees' => 'Number of Employees',
	'Not Contacted'=>'Not Contacted',
	'Phone' => 'Primary Phone',
	'Pre Qualified'=>'Pre Qualified',
	'Prof.'=>'Prof.',
	'Qualified'=>'Qualified',
	'Secondary Email' => 'Secondary Email',
	'SINGLE_CustomLeads' => 'CustomLead',
	'Warm'=>'Warm',
	'Website' => 'Website',
	'Rating' => 'Potential',
);

$jsLanguageStrings = array(
	'JS_SELECT_CONTACTS' => 'Select Contacts to proceed',
	'JS_SELECT_ORGANIZATION_OR_CONTACT_TO_CONVERT_CUSTOMLEAD' => 'Conversion requires selection of Contact or Organization',
	'JS_SELECT_ORGANIZATION' => 'Select Organization to proceed',
);