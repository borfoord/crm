<?php
/*+**********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 ************************************************************************************/

$languageStrings = Array(
	'DragNDrop' => 'DragNDrop',
	'SINGLE_DragNDrop' => 'DragNDrop',
	'DragNDrop ID' => 'DragNDrop ID',
	'LBL_CUSTOM_INFORMATION' => 'Custom Information',
	'LBL_DRAGNDROP_INFORMATION' => 'DragNDrop Log Information',
	'LBL_DDD_TITLE' => 'DragNDrop',
        'LBL_DDD_SETTINGS' => 'DragNDrop',
        'LBL_STATUS' => 'Status',
        'LBL_SETTINGS' => 'Settings',
	'LBL_DRAGNDROP_SETTINGS' => 'DragNDrop Settings',
	'LBL_DRAGNDROP_LOGS' => 'DragNDrop Logs',
        'LBL_LIST_MODULES' => 'List of Modules related to Documents Module',
	'LBL_DOCUMENT_FOLDERS' => 'Document Folders',
);

?>
