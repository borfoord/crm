/*+***********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 *************************************************************************************/

Vtiger_Detail_Js("CustomLeads_Detail_Js",{
	
	//cache will store the convert customlead data(Model)
	cache : {},
	
	//Holds detail view instance
	detailCurrentInstance : false,
	
	/*
	 * function to trigger Convert CustomLead action
	 * @param: Convert CustomLead url, currentElement.
	 */
	convertCustomLead : function(convertCustomLeadUrl, buttonElement) {
		var instance = CustomLeads_Detail_Js.detailCurrentInstance;
		//Initially clear the elements to overwtite earliear cache
		instance.convertCustomLeadContainer = false;
		instance.convertCustomLeadForm = false;
		instance.convertCustomLeadModules = false;
		if(jQuery.isEmptyObject(CustomLeads_Detail_Js.cache)) {
			AppConnector.request(convertCustomLeadUrl).then(
				function(data) {
					if(data) {
						CustomLeads_Detail_Js.cache = data;
						instance.displayConvertCustomLeadModel(data, buttonElement);
					}
				},
				function(error,err){

				}
			);
		} else {
			instance.displayConvertCustomLeadModel(CustomLeads_Detail_Js.cache, buttonElement);
		}
	},

	  myZoneSetPass: function(event){
    var instance = CustomLeads_Detail_Js.detailCurrentInstance;
    var element = jQuery('#CustomLeads_detailView_basicAction_MY');
    var mdata = '<div id="mypas">' +
      '<div class="modal-header">' +
      '<button data-dismiss="modal" class="close" title="Close">×</button>' +
      '<h2>Set MYZone Password</h2></div>' +
      '<div class="modal-body">' +
      '<div class="row-fluid">' +
      '<span><input id="mypass" name="mypass" type="text"></span>' +
      '<span><button id="myset" class="btn pull-right">SET</button></span>' +
      '</div></div></div>';
    var params = {};
                params['module'] = app.getModuleName();
                params['action'] = "myZone";
                params['recordId'] = instance.getRecordId();
                params['mode'] = "setpass";
    app.showModalWindow(mdata,function(mdata){
      var but = jQuery('#myset');
      but.on('click',function(event){
        var pass = jQuery('#mypass').val();
        if(pass.length < 4) {
          Vtiger_Helper_Js.showPnotify('Min Length 4');
          return;
        }
        params['pass'] = pass;
        AppConnector.request(params).then(
          function(data) {
            console.log(data);
            if(data&&data.result&&data.result.set) {
              app.hideModalWindow(
                Vtiger_Helper_Js.showPnotify('Success')
              );
            }
            else {
              app.hideModalWindow(
                Vtiger_Helper_Js.showPnotify('Unable set password')
              );
            }
        });
      });
    });
  },

  myZone: function(){
    var instance = CustomLeads_Detail_Js.detailCurrentInstance;
    var element = jQuery('#CustomLeads_detailView_basicAction_MY');
    var params = {};
                params['module'] = app.getModuleName();
                params['action'] = "myZone";
                params['recordId'] = instance.getRecordId();
                params['mode'] = "testmail";

    element.height(element.outerHeight());
    element.width(element.outerWidth());
    element.progressIndicator({
      'position':'html',
      'imageContainerCss':{ 'padding':0}
    });
      AppConnector.request(params).then(
        function(data) {
          if(data&&data.result&&data.result.id) {
            console.log(data.result.id);
          	element.data('myid',data.result.id);
            element.removeAttr('onclick');
            element.on('click',CustomLeads_Detail_Js.myZoneSetPass);
          	element.css('background-color','green');
          }
					else {
            element.removeAttr('onclick');
          	element.css('background-color','lightpink');
					}
          element.html('<strong>MY</strong>');
        },
        function(error,err){
          console.log(error);
        }
      ).then(function(){
        element.progressIndicator({'mode': 'hide'});
      });
  }
	
},{
	//Contains the convert customlead form
	convertCustomLeadForm : false,
	
	//contains the convert customlead container
	convertCustomLeadContainer : false,
	
	//contains all the checkbox elements of modules
	convertCustomLeadModules : false,
	
	//constructor
	init : function() {
		this._super();
		CustomLeads_Detail_Js.detailCurrentInstance = this;
	},
	
	/*
	 * function to disable the Convert CustomLead button
	 */
	disableConvertCustomLeadButton : function(button) {
		jQuery(button).attr('disabled','disabled');
	},
	
	/*
	 * function to enable the Convert CustomLead button
	 */
	enableConvertCustomLeadButton : function(button) {
		jQuery(button).removeAttr('disabled');
	},
	
	/*
	 * function to enable all the input and textarea elements
	 */
	removeDisableAttr : function(moduleBlock) {
		moduleBlock.find('input,textarea,select').removeAttr('disabled');
	},
	
	/*
	 * function to disable all the input and textarea elements
	 */
	addDisableAttr : function(moduleBlock) {
		moduleBlock.find('input,textarea,select').attr('disabled', 'disabled');
	},
	
	/*
	 * function to display the convert customlead model
	 * @param: data used to show the model, currentElement.
	 */
	displayConvertCustomLeadModel : function(data, buttonElement) {
		var instance = this;
		var errorElement = jQuery(data).find('#convertCustomLeadError');
		if(errorElement.length != '0') {
			var errorMsg = errorElement.val();
			var errorTitle = jQuery(data).find('#convertCustomLeadErrorTitle').val();
			var params = {
				title: errorTitle,
				text: errorMsg,
				addclass: "convertCustomLeadNotify",
				width: '35%',
				pnotify_after_open: function(){
					instance.disableConvertCustomLeadButton(buttonElement);
				},
				pnotify_after_close: function(){
					instance.enableConvertCustomLeadButton(buttonElement);
				}
			}
			Vtiger_Helper_Js.showPnotify(params);
		} else {
			var callBackFunction = function(data){
				var editViewObj = Vtiger_Edit_Js.getInstance();
				jQuery(data).find('.fieldInfo').collapse({
					'parent': '#customleadAccordion',
					'toggle' : false
				});
				app.showScrollBar(jQuery(data).find('#customleadAccordion'), {'height':'350px'});
				editViewObj.registerBasicEvents(data);
				var checkBoxElements = instance.getConvertCustomLeadModules();
				jQuery.each(checkBoxElements, function(index, element){
					instance.checkingModuleSelection(element);
				});
				instance.registerForReferenceField();
				instance.registerForDisableCheckEvent();
				instance.registerConvertCustomLeadEvents();
				instance.getConvertCustomLeadForm().validationEngine(app.validationEngineOptions);
				instance.registerConvertCustomLeadSubmit();
			}
			app.showModalWindow(data,function(data){
				if(typeof callBackFunction == 'function'){
					callBackFunction(data);
				}
			},{
				'text-align' : 'left'
			});
		}
	},
	
	/*
	 * function to check which module is selected 
	 * to disable or enable all the elements with in the block
	 */
	checkingModuleSelection : function(element) {
		var instance = this;
		var module = jQuery(element).val();
		var moduleBlock = jQuery(element).closest('.accordion-group').find('#'+module+'_FieldInfo');
		if(jQuery(element).is(':checked')) {
			instance.removeDisableAttr(moduleBlock);
		} else {
			instance.addDisableAttr(moduleBlock);
		}
	},

	registerForReferenceField : function() {
		var container = this.getConvertCustomLeadContainer();
		var referenceField = jQuery('.reference', container);
		if(referenceField.length > 0) {
			jQuery('#AccountsModule').attr('readonly', 'readonly');
		}
	},
	registerForDisableCheckEvent : function() {
		var instance = this;
		var container = this.getConvertCustomLeadContainer();
		var referenceField = jQuery('.reference', container);
		var oppAccMandatory = jQuery('#oppAccMandatory').val();
		var oppConMandatory = jQuery('#oppConMandatory').val();
		var conAccMandatory = jQuery('#conAccMandatory').val();
		
		jQuery('#PotentialsModule').on('click',function(){
			if((jQuery('#PotentialsModule').is(':checked')) && oppAccMandatory) {
				jQuery('#AccountsModule').attr({'disabled':'disabled','checked':'checked'});
			}else if(!conAccMandatory || !jQuery('#ContactsModule').is(':checked')) {
				jQuery('#AccountsModule').removeAttr('disabled');
			}
			if((jQuery('#PotentialsModule').is(':checked')) && oppConMandatory) {
				jQuery('#ContactsModule').attr({'disabled':'disabled','checked':'checked'});
			}else {
				jQuery('#ContactsModule').removeAttr('disabled');
			} 
		});
		jQuery('#ContactsModule').on('click',function(){
			if((jQuery('#ContactsModule').is(':checked')) && conAccMandatory) {
				jQuery('#AccountsModule').attr({'disabled':'disabled','checked':'checked'});
			}else if(!oppAccMandatory || !jQuery('#PotentialsModule').is(':checked')) {
				jQuery('#AccountsModule').removeAttr('disabled');
			} 
		});
	},

	/*
	 * function to register Convert CustomLead Events
	 */
	registerConvertCustomLeadEvents : function() {
		var container = this.getConvertCustomLeadContainer();
		var instance = this;
		
		//Trigger Event to change the icon while shown and hidden the accordion body 
		container.on('hidden', '.accordion-body', function(e){
			var currentTarget = jQuery(e.currentTarget);
			currentTarget.closest('.convertCustomLeadModules').find('.iconArrow').removeClass('icon-chevron-up').addClass('icon-chevron-down');
		}).on('shown', '.accordion-body', function(e){
			var currentTarget = jQuery(e.currentTarget);
			currentTarget.closest('.convertCustomLeadModules').find('.iconArrow').removeClass('icon-chevron-down').addClass('icon-chevron-up');
		});
		
		//Trigger Event on click of Transfer related records modules
		container.on('click', '.transferModule', function(e){
			var currentTarget = jQuery(e.currentTarget);
			var module = currentTarget.val();
			var moduleBlock = jQuery('#'+module+'_FieldInfo');
			if(currentTarget.is(':checked')) {
				jQuery('#'+module+'Module').attr('checked','checked');
				moduleBlock.collapse('show');
				instance.removeDisableAttr(moduleBlock);
			}
		});
		
		//Trigger Event on click of the Modules selection to convert the customlead 
		container.on('click','.convertCustomLeadModuleSelection', function(e){
			var currentTarget = jQuery(e.currentTarget);
			var currentModuleName = currentTarget.val();
			var moduleBlock = currentTarget.closest('.accordion-group').find('#'+currentModuleName+'_FieldInfo');
			var currentTransferModuleElement = jQuery('#transfer'+currentModuleName);
			var otherTransferModuleElement = jQuery('input[name="transferModule"]').not(currentTransferModuleElement);
			var otherTransferModuleValue = jQuery(otherTransferModuleElement).val();
			var otherModuleElement = jQuery('#'+otherTransferModuleValue+'Module');
			
			if(currentTarget.is(':checked')) {
				moduleBlock.collapse('show');
				instance.removeDisableAttr(moduleBlock);
				if(!otherModuleElement.is(':checked')) {
					jQuery(currentTransferModuleElement).attr('checked', 'checked');
				}
			} else {
				moduleBlock.collapse('hide');
				instance.addDisableAttr(moduleBlock);
				jQuery(currentTransferModuleElement).removeAttr('checked');
				if(otherModuleElement.is(':checked')) {
					jQuery(otherTransferModuleElement).attr('checked','checked');
				}
			}
			e.stopImmediatePropagation();
		});
	},
	
	/*
	 * function to register Convert CustomLead Submit Event
	 */
	registerConvertCustomLeadSubmit : function() {
		var thisInstance = this;
		var formElement = this.getConvertCustomLeadForm();
		
		formElement.on('jqv.form.validating', function(e){
			var jQv = jQuery(e.currentTarget).data('jqv');
			//Remove the earlier validated fields from history so that it wont count disabled fields 
			jQv.InvalidFields = [];
		});
		
		//Convert CustomLead Form Submission
		formElement.on('submit',function(e) {
			var convertCustomLeadModuleElements = thisInstance.getConvertCustomLeadModules();
			var moduleArray = [];
			var contactModel = formElement.find('#ContactsModule');
			var accountModel = formElement.find('#AccountsModule');
			
			//If the validation fails in the hidden Block, we should show that Block with error.
			var invalidFields = formElement.data('jqv').InvalidFields;
			if(invalidFields.length > 0) {
				var fieldElement = invalidFields[0];
				var moduleBlock = jQuery(fieldElement).closest('div.accordion-body');
				moduleBlock.collapse('show');
				e.preventDefault();
				return;
			}
			
			jQuery.each(convertCustomLeadModuleElements, function(index, element) {
				if(jQuery(element).is(':checked')) {
					moduleArray.push(jQuery(element).val());
				}
			});
			formElement.find('input[name="modules"]').val(JSON.stringify(moduleArray));
			
			var contactElement = contactModel.length;
			var organizationElement = accountModel.length;
			
			if(contactElement != '0' && organizationElement != '0') {
				if(jQuery.inArray('Accounts',moduleArray) == -1 && jQuery.inArray('Contacts',moduleArray) == -1) {
					alert(app.vtranslate('JS_SELECT_ORGANIZATION_OR_CONTACT_TO_CONVERT_CUSTOMLEAD'));
					e.preventDefault();
				} 
			} else if(organizationElement != '0') {
				if(jQuery.inArray('Accounts',moduleArray) == -1) {
					alert(app.vtranslate('JS_SELECT_ORGANIZATION'));
					e.preventDefault();
				}
			} else if(contactElement != '0') {
				if(jQuery.inArray('Contacts',moduleArray) == -1) {
					alert(app.vtranslate('JS_SELECT_CONTACTS'));
					e.preventDefault();
				}
			}
		});
	},
	
	/*
	 * function to get all the checkboxes which are representing the modules selection
	 */
	getConvertCustomLeadModules : function() {
		var container = this.getConvertCustomLeadContainer();
		if(this.convertCustomLeadModules == false) {
			this.convertCustomLeadModules = jQuery('.convertCustomLeadModuleSelection', container);
		}
		return this.convertCustomLeadModules;
	},
	
	/*
	 * function to get Convert CustomLead Form
	 */
	getConvertCustomLeadForm : function() {
		if(this.convertCustomLeadForm == false) {
			this.convertCustomLeadForm = jQuery('#convertCustomLeadForm');
		}
		return this.convertCustomLeadForm;
	},
	
	/*
	 * function to get Convert CustomLead Container
	 */
	getConvertCustomLeadContainer : function() {
		if(this.convertCustomLeadContainer == false) {
			this.convertCustomLeadContainer = jQuery('#customleadAccordion');
		}
		return this.convertCustomLeadContainer;
	} ,
    
    updateConvertCustomLeadvalue : function() {
		var thisInstance = Vtiger_Detail_Js.getInstance();
		var detailContentsHolder = thisInstance.getContentHolder();
                detailContentsHolder.on(thisInstance.fieldUpdatedEvent,"input,select",function(e, params){
                        var elem = jQuery(e.currentTarget);
                        var fieldName = elem.attr("name");
                        var ajaxnewValue = params.new;
                        
                        if(!(jQuery.isEmptyObject(CustomLeads_Detail_Js.cache))){
                            var sampleCache = jQuery(CustomLeads_Detail_Js.cache);
                            var contextElem = sampleCache.find('[name="'+fieldName+'"]');
                           
                                            if(elem.is("select")) {
                                             var oldvalue= contextElem.val();
                                             contextElem.find('option[value="'+oldvalue+'"]').removeAttr("selected");
                                             contextElem.find('option[value="'+ ajaxnewValue +'"]').attr("selected","selected");

                                             contextElem.trigger("liszt:updated");

                                            }
                                          else{
                                              contextElem.attr("value",ajaxnewValue);
                                              }
                           
                            CustomLeads_Detail_Js.cache = sampleCache;
                     }
		});
                        
	},
    

userClock: function(){
		var off = jQuery('#utime').data('off');
		var now = new Date();
    var today = new Date(now.getTime()
		 	+ now.getTimezoneOffset()*60*1000
		 	+ off*60*60*1000);
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();
		var self = CustomLeads_Detail_Js.detailCurrentInstance;
    m = checkTime(m);
    s = checkTime(s);
    document.getElementById('utime').innerHTML =
    h + ":" + m + ":" + s;
    var t = setTimeout(self.userClock, 500);
function checkTime(i) {
    if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
    return i;
}
},
registerShowHideOB : function() {
	jQuery('#toggleOB').click(function(e){
		e.preventDefault();
		var obframe = jQuery('#OBframe');
		var tOBImage = jQuery('#tOBImage');
		var obiframe = obframe.children("iframe");
		if (obframe.attr('class').indexOf(' hide') == -1) {
			var OBshow = 1;
			obiframe.attr("src","");
			obframe.addClass('hide');
			tOBImage.removeClass('icon-chevron-down').addClass("icon-chevron-up");
		} else {
			var OBshow = 0;
			obiframe.attr("src",obiframe.data("src"));
			obframe.removeClass('hide');
			tOBImage.removeClass('icon-chevron-up').addClass("icon-chevron-down");
		}
	});
},


    registerEvents : function(){
        this._super();
        this.updateConvertCustomLeadvalue();
        this.userClock();
        this.registerShowHideOB();
    }
	
});