{*<!--
/*********************************************************************************
  ** The contents of this file are subject to the vtiger CRM Public License Version 1.0
   * ("License"); You may not use this file except in compliance with the License
   * The Original Code is:  vtiger CRM Open Source
   * The Initial Developer of the Original Code is vtiger.
   * Portions created by vtiger are Copyright (C) vtiger.
   * All Rights Reserved.
  *
 ********************************************************************************/
-->*}

{* To check current user has permission to make outbound call. If so, make all the detail view phone fields as links to call *}
{assign var=MODULE value='PBXManager'}
{assign var=MODULEMODEL value=Vtiger_Module_Model::getInstance($MODULE)}
{assign var=FIELD_VALUE value=$FIELD_MODEL->get('fieldvalue')}
{if $MODULEMODEL and $MODULEMODEL->isActive() and $FIELD_VALUE}
    {assign var=PERMISSION value=PBXManager_Server_Model::checkPermissionForOutgoingCall()}
    {if $PERMISSION}
        {assign var=PHONE_FIELD_VALUE value=$FIELD_VALUE}
        {assign var=PHONE_NUMBER value=$PHONE_FIELD_VALUE|regex_replace:"/[-()\s]/":""}
        {assign var=AGENTID value=$CURRENT_USER_MODEL->get('agentid')}
        {assign var=SERVERNAME value={$CURRENT_USER_MODEL->get('servername')|replace:'{AGENTID}':$AGENTID|replace:'{PHONENUMBER}':$PHONE_NUMBER}}                        
        {$FIELD_MODEL->get('fieldvalue')}<a href="#" class="phoneField" data-value="{$PHONE_NUMBER}" record="{$RECORD->getId()}" onclick="window.open('{$SERVERNAME}');"> <img src="/layouts/vlayout/modules/CustomLeads/resources/sprites/phone-icon.png" alt="phone" style="width:16px;height:16px;margin:auto;"></a>
    {else}
        {$FIELD_MODEL->getDisplayValue($FIELD_MODEL->get('fieldvalue'), $RECORD->getId(), $RECORD)}
    {/if}
{else}
    {$FIELD_MODEL->getDisplayValue($FIELD_MODEL->get('fieldvalue'), $RECORD->getId(), $RECORD)}
{/if}
