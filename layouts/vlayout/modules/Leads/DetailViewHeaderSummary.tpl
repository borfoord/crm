{strip}
{assign var=MT4ACCS value=$RECORD->DetailViewHeaderSummary()}
{if $MT4ACCS }
<table class="lead-fin-summary">
<tr>
<th>acc</th>
<th>Deposit</th>
<th>Withdraw</th>
<th>Net</th>
<th>Credit</th>
<th>Balance</th>
<th>P&L</th>
<th>Volume(lot)</th>
<th>AVG Lot Forex</th>
<th>AVG Lot CFD</th>
<th>AVG Lot Indexes</th>
</tr>
	{*"{$MT4ACCS|@debug_print_var}"*}
	{foreach item=MT4ACC from=$MT4ACCS}
<tr>
{if $MT4ACC->mt4group }
<td class="mt4live">{$MT4ACC->login}</td>
{else}
<td>{$MT4ACC->login}</td>
{/if}
<td>{$MT4ACC->deposit}</td>
<td>{-$MT4ACC->withdraw}</td>
<td>{$MT4ACC->depo}</td>
<td>{$MT4ACC->credit}</td>
<td>{$MT4ACC->balance}</td>
<td>{$MT4ACC->prof}</td>
<td>{$MT4ACC->vl}</td>
<td>{$MT4ACC->avf}</td>
<td>{$MT4ACC->avc}</td>
<td>{$MT4ACC->avi}</td>
</tr>
  {/foreach}
</table>
{/if}
{/strip}
