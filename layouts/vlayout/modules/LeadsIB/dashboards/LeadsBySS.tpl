{*<!--
/*********************************************************************************
  ** The contents of this file are subject to the vtiger CRM Public License Version 1.0
   * ("License"); You may not use this file except in compliance with the License
   * The Original Code is:  vtiger CRM Open Source
   * The Initial Developer of the Original Code is vtiger.
   * Portions created by vtiger are Copyright (C) vtiger.
   * All Rights Reserved.
  *
 ********************************************************************************/
-->*}
<script type="text/javascript">

	// Vtiger_Barchat_Widget_Js('Vtiger_Leadsbysource_Widget_Js',{},{});

	Vtiger_Widget_Js('Vtiger_Leadsbyss_Widget_Js',{},{
		 postLoadWidget: function() {
		this._super();
        var thisInstance = this;
		 parent = $("#{$HEADER_WIDGET_ID}").parent(); 
		 app.showSelect2ElementView(parent.find('select.select2'));
        
		 }
	 });


</script>

<div class="dashboardWidgetHeader" id={$HEADER_WIDGET_ID} >
	{include file="dashboards/WidgetHeader.tpl"|@vtemplate_path:$MODULE_NAME SETTING_EXIST=true}
	<div class="row-fluid filterContainer hide" style="position:absolute;z-index:100001">
		<div class="row-fluid">
			<span class="span5">
				<span class="pull-right">
					{vtranslate('Created Time', $MODULE_NAME)} &nbsp; {vtranslate('LBL_BETWEEN', $MODULE_NAME)}
				</span>
			</span>
			<span class="span4">
				<input type="text" name="createdtime" class="dateRange widgetFilter"  
				{if isset($DATEOUT)}
				    {$DATEOUT}
				{/if} />
			</span>	
		</div>
		<div class="row-fluid">		
			<span class="span5">
				<span class="pull-right">
					{vtranslate('Assigned To', $MODULE_NAME)}
				</span>
			</span>
			<span class="span4" >
				{assign var=CURRENT_USER_ID value=$CURRENTUSER->getId()}
				<select class="widgetFilter" name="smownerid">
				{if ($IFGROUPUSE == 1)}
					<option value="">{vtranslate('LBL_ALL', $MODULE_NAME)}</option>
					<optgroup label="Users">
					{foreach key=USER_ID item=USER_NAME from=$ARRWITHUSERS}
					<option value="{$USER_ID}"
						{if $USER_ID eq $SMOWNERID	}
                            selected							
						{/if}
					>
						{if $USER_ID eq $CURRENTUSER->getId()}
							{vtranslate('LBL_MINE',$MODULE_NAME)}
						{else}
							{$USER_NAME}
						{/if}
					</option>
					{/foreach}
					</optgroup>
				
					<optgroup label="Group">
					{foreach key=USER_ID item=USER_NAME from=$ARRWITHGROUP}
					<option value="{$USER_ID}"
						{if $USER_ID eq $SMOWNERID	}
                            selected							
						{/if}
					>
					{$USER_NAME}
					</option>
					{/foreach}
					</optgroup>
				
				{else}
					<option value="">{vtranslate('LBL_ALL', $MODULE_NAME)}</option>
					{foreach key=USER_ID item=USER_NAME from=$ACCESSIBLE_USERS}
					<option value="{$USER_ID}"
						{if $USER_ID eq $SMOWNERID	}
                            selected							
						{/if}
					>
						{if $USER_ID eq $CURRENTUSER->getId()}
							{vtranslate('LBL_MINE',$MODULE_NAME)}
						{else}
							{$USER_NAME}
						{/if}
					</option>
					{/foreach}
				{/if} 

					
				</select>
			</span>
		</div>

		
		<div class="row-fluid">		
			<span class="span5">
				<span class="pull-right">
					{vtranslate('Data Settings', $MODULE_NAME)}
				</span>
			</span>
			<span class="span4" >
				{assign var=CURRENT_USER_ID value=$CURRENTUSER->getId()}
				<select class="widgetFilter" name="datasettings">
					<option value="0" {if $DATASETTINGS eq '0' } selected {/if} >{vtranslate('All Rows&Colls', $MODULE_NAME)}</option>
					<option value="1" {if $DATASETTINGS eq '1' } selected {/if} >{vtranslate('No Epmpty Rows', $MODULE_NAME)}</option>
					<option value="2" {if $DATASETTINGS eq '2' } selected {/if} >{vtranslate('No Epmpty Cols', $MODULE_NAME)} </option>
					<option value="3" {if $DATASETTINGS eq '3' } selected {/if} >{vtranslate('No Epmpty Rows&Colls', $MODULE_NAME)} </option>
					</option>
				</select>
			</span>
		</div>




		<div class="row-fluid">		
			<span class="span5">
				<span class="pull-right">
					{vtranslate('Good Statuses', $MODULE_NAME)}
				</span>
			</span>
			<span class="span4" >
				{assign var=CURRENT_USER_ID value=$CURRENTUSER->getId()}
{literal}
<select class="select2 listSearchContributor widgetFilter" style="width:40%" name="goodleadstatus" multiple  data-fieldinfo='{"mandatory":false,"presence":true,"quickcreate":false,"masseditable":true,"defaultvalue":false,"type":"picklist","name":"goodleadstatus"}'>
{/literal}	
{foreach key=KEY item=VALUE from=$SELECT2STATUSVALUE}
	<option value="{$VALUE}" {if $SELECT2STATUSVALUESELECTED|is_array} {if $VALUE|in_array:$SELECT2STATUSVALUESELECTED}   selected	{/if} {/if} > {$VALUE}</option>
{/foreach}
</select>

			</span>
		</div>

		
		
	</div>
</div>
<div class="dashboardWidgetContent">
	{include file="dashboards/LeadsBySSContents.tpl"|@vtemplate_path:$MODULE_NAME}
</div>

		<script type="text/javascript">
jQuery(document).ready(function(){
    
    $("#mybuttonforclick").click();
});
</script>
