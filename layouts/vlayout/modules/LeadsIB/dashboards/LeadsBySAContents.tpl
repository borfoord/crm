    <table class="table table-bordered listViewEntriesTable" >
        <tr><th  > &nbsp; </th>
            {foreach key=I item=X from=$ARRX}
             <th style="text-align:center;font-weight:normal;" {if $SELECT2STATUSVALUESELECTED|is_array} {if ($I|in_array:$SELECT2STATUSVALUESELECTED and $I != "") or ($X|in_array:$SELECT2STATUSVALUESELECTED) }  class="good" {else} class="bad"	{/if} {/if} > {$X}    </th> 
            {/foreach}
            <th style="text-align:center;" > Total </th><th style="text-align:center;" class="good"> Good</th><th style="text-align:center;" class="bad"> Bad</th><th style="text-align:center;">   Client %</th>
        </tr>    
            {foreach key=J item=Y from=$ARRY}
        <tr >
                <td  class="fieldLabel" > {$Y} </td>
                {foreach key=I item=X from=$ARRX}
                <td  {$OUTARRAY.$X.$Y.style}  class="fields" > <a style="width:100%;display:block;" href={$OUTARRAY.$X.$Y.links}> {$OUTARRAY.$X.$Y.value} </a> </td>
                {/foreach}

                <td class="fields" style="text-align:center;font-weight:bold;    background-color: #f5f5f5;"> <a style="width:100%;display:block;" href={$ARRSUMX.$Y.links}>   {$ARRSUMX.$Y.value}  </a> </td>
                <td class="fields" style="text-align:center;font-weight:normal;"><b>{if $ARRSUMX.$Y.value != 0} {round($GOODARRAY.$Y/$ARRSUMX.$Y.value*10000)/100}% 	{/if} </b>  <!-- <br/> ({$GOODARRAY.$Y}) --> </td>
                <td class="fields" style="text-align:center;font-weight:normal;"><b>{if $ARRSUMX.$Y.value != 0} {round($BADARRAY.$Y/$ARRSUMX.$Y.value*10000)/100}% 	{/if} </b>   <!-- <br/> ({$BADARRAY.$Y}) --> </td>
                <td class="fields" style="text-align:center;font-weight:normal;"><b>{if $ARRSUMX.$Y.value != 0} {round($OUTARRAY.{Client}.$Y.value/$ARRSUMX.$Y.value*10000)/100}% 	{/if} </b>   <!-- <br/> ({$BADARRAY.$Y}) --> </td>
                
        </tr>
            {/foreach}

            <tr >
                <td class="fieldLabel" style="font-weight:bold;"> Total </td>
                    {foreach key=I item=X from=$ARRX}
                    <td class="fields" style="text-align:center;font-weight:bold;background-color:#f5f5f5;" > <a style="width:100%;display:block;" href={$ARRSUMY.$X.links}>  {$ARRSUMY.$X.value}  </a>  </td>
                    {/foreach}
                <td class="fields" style="text-align:center;font-weight:bold;background-color:#f5f5f5;"> {$ALLTOTAL} </td>
                <td class="fields" style="text-align:center;font-weight:bold;background-color:#f5f5f5;">{if $ALLTOTAL != 0} {round( $ALLGOODLEADS/$ALLTOTAL*10000) / 100 }%<br>({$ALLGOODLEADS})	{/if}</td>
                <td class="fields" style="text-align:center;font-weight:bold;background-color:#f5f5f5;">{if $ALLTOTAL != 0}  {round( $ALLBADLEADS/$ALLTOTAL*10000) / 100 }%<br>({$ALLBADLEADS})   	{/if}</td>
                <td class="fields" style="text-align:center;font-weight:bold;background-color:#f5f5f5;">  </td>
                
            </tr>
    </table>
		
		
	{literal}
<style>

th.bad {
    color: red;
}
th.good {
    color: green;
}

    .fieldLabel {
           width:5%;
    }
    .fieldLabel {
        width:15%;
    }
    .slimScrollDiv
    {height:auto !important;}
     .dashboardWidgetContent
    {height:auto !important;;}
</style>
{/literal}		
		
	<script type="text/javascript">

	jQuery(document).ready(function(){
	
	    var parHeight = {$YWIDZET}*100 - 50;
	
		var headerIdEl = $("#{$HEADER_WIDGET_ID_WITH_DATA}");
		headerIdEl.text("");
		
		if ( ("{$SMOWNERID}").length ) {
			headerIdEl.text(" by "  + "{$ACCESSIBLE_USERS.$SMOWNERID}" );
		}	
	
		if ( ("{$DATECAPTION}").length  ) {
				headerIdEl.text(headerIdEl.text() + " "  + "{$DATECAPTION}");
			}	
			
			
		parent = $("#{$HEADER_WIDGET_ID}").parent();
		Vtiger_DashBoard_Js.gridster.resize_widget($(parent),{$XWIDZET},{$YWIDZET});



    	parent.height(parHeight+50);	
		$(parent.children()[1]).height(parHeight);
		$($(parent.children()[1]).children()[0]).height(parHeight);
		$($($(parent.children()[1]).children()[0]).children()[0]).height(parHeight);
        parent.find('table.listViewEntriesTable').height(parHeight);


	});
		
				
</script>
	
