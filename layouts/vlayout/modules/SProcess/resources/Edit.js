Vtiger_Edit_Js("SProcess_Edit_Js",{},{

		/**
		 ** Function to register event for copying address between two fileds
		 **/
registerEventForCopyingSaleProcess : function(container){
var thisInstance = this;
var target = document.querySelectorAll('[name="spservice_display"]');
var observer = new MutationObserver(function(mutations) {
	mutations.forEach(function(mutation) {
		var t = $(mutation.target).parents('table');
		var sn = $(t).find('[name="spname"]');
		if(!$(sn).val()){$(sn).val($(mutation.target).val());}      
		});
	});
var config = { attributes: true, attributeFilter: ["readonly"] };
target.forEach(function(t){
	observer.observe(t, config);
	});

},

registerBasicEvents : function(container){
												this._super(container);
												this.registerEventForCopyingSaleProcess(container);
											}
})
