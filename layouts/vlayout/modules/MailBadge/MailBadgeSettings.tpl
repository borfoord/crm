<link rel="stylesheet" type="text/css" href="modules/{$MODULE_NAME}/public/css/font-awesome.css" media="screen" />
<link rel="stylesheet" type="text/css" href="modules/{$MODULE_NAME}/public/css/badge.css" media="screen" />

<div class = 'mailbadge_settings_container'>
	{if $smarty.request.saved eq true}
		<div id = 'mailbadge_settings_notification' class = 'alert alert-success' style = 'text-align: center;'> Settings Successfully Stored <i class = 'fa fa-times-circle-o fa-lg' style = 'float: right; top: 3px; position: relative;' onclick = 'jQuery("#mailbadge_settings_notification").hide()'></i> </div>
	{/if}
	<div class = 'mailbadge_header'> 
		<h2> <i class = 'fa fa-envelope-o fa-lg'> </i> <span style = 'padding-left: 5px;'> {$MODULE_NAME} </span> </h2>
	</div>
	<div class = 'mailbadge_container'>
		<form action = 'index.php?module={$MODULE_NAME}&action=SaveSettings' method = 'POST' name = 'badge_settings'>
		<div>
			<input type = 'checkbox' name = 'badge_status' id = 'badge_status' {$BADGE_STATUS} class = 'iconic-check'>
                     	<label for = "badge_status" style = 'font-size: 1.5em; top: 5px; position: relative; left: 10px;'> {$MODULE_NAME} status </label>
		</div>
		<div class = 'mailbadge_badge_color'>
			<input type = 'color' name = 'badge_color' id = 'badge_color' value = '{$BADGE_COLOR}'>
			<label for = 'badge_color' style = 'float: right;'> Badge Color </label>
		</div>
		<div class = 'mailbadge_badge_submit'>
			<button type = 'submit' class = 'btn btn-primary' onclick = 'closeColorBox()'> Save </button>
		</div>
		</form>
	</div>
</div>
