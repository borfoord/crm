{*<!--
/*********************************************************************************
** The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
*
 ********************************************************************************/
-->*}
{strip}
{*if isset($CURRENT_USER_MODEL) && $MODULE_NAME eq "Leads" && $VIEW eq "Detail" && ($CURRENT_USER_MODEL->getRole() eq "H2" || $CURRENT_USER_MODEL->getRole() eq "H16" || $CURRENT_USER_MODEL->getRole() eq "H40" || $CURRENT_USER_MODEL->getRole() eq "H27")*}
{if isset($CURRENT_USER_MODEL) && $MODULE_NAME eq "Leads" && $VIEW eq "Detail" }
<div id="toggleOB" class="toggleOB" title="OmniBrowse">
        <i id="tOBImage" class="{if $LEFTPANELHIDE neq '1'}icon-chevron-down{else}icon-chevron-up{/if}"></i>
</div>
<div class="row-fluid hide" id="OBframe">
<iframe src="" data-src="/omnibrowse_launch.html?smat={$CURRENT_USER_MODEL->get('smat')}&extId={$RECORD->get('myzoneid')}" style="width:100%" onload="this.height=window.innerHeight" smat="{$CURRENT_USER_MODEL->get('smat')}"></iframe>
</div>
{/if}
		<input id='activityReminder' class='hide noprint' type="hidden" value="{$ACTIVITY_REMINDER}"/>

		{* Feedback side-panel button *}
		{if $HEADER_LINKS && $MAIN_PRODUCT_SUPPORT && !$MAIN_PRODUCT_WHITELABEL}
		{assign var="FIRSTHEADERLINK" value=$HEADER_LINKS.0}
		{assign var="FIRSTHEADERLINKCHILDRENS" value=$FIRSTHEADERLINK->get('childlinks')}
		{assign var="FEEDBACKLINKMODEL" value=$FIRSTHEADERLINKCHILDRENS.2}
		<div id="userfeedback" class="feedback noprint">
			<a href="https://discussions.vtiger.com" target="_blank" xonclick="{$FEEDBACKLINKMODEL->get('linkurl')}" class="handle">{vtranslate("LBL_FEEDBACK", "Vtiger")}</a>
		</div>
		{/if}

		{if !$MAIN_PRODUCT_WHITELABEL && isset($CURRENT_USER_MODEL)}
		<footer class="noprint">
                    <div class="vtFooter">
			<p>
				{vtranslate('POWEREDBY')} {$VTIGER_VERSION} &nbsp;
				&copy; 2004 - {date('Y')}&nbsp&nbsp;
				<a href="//www.vtiger.com" target="_blank">vtiger.com</a>
				&nbsp;|&nbsp;
				<a href="#" onclick="window.open('copyright.html','copyright', 'height=115,width=575').moveTo(210,620)">{vtranslate('LBL_READ_LICENSE')}</a>
				&nbsp;|&nbsp;
				<a href="https://www.vtiger.com/crm/privacy-policy" target="_blank">{vtranslate('LBL_PRIVACY_POLICY')}</a>
			</p>
                     </div>
		</footer>
		{/if}
		
		{* javascript files *}
		{include file='JSResources.tpl'|@vtemplate_path}
		</div>
		
	</body>
</html>
{/strip}
