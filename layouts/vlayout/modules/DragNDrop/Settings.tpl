{include file='DDDHeader.tpl'|@vtemplate_path:$MODULE}
<form action = 'index.php?module={$MODULE}&action=SaveSettings' method = 'POST' name = 'savesettings'>
<div class="contentsDiv span10 marginLeftZero" style = 'width: 95%; '>
	<div class="contents row-fluid">
		<div class="widget_header row-fluid">
			<div class="span8">
				<h3>{vtranslate($DDD_SETTINGS_TITLE, $MODULE)} {$version}</h3>
			</div>
			<div class="span4">
				<div class="pull-right">
					<button class="btn editButton" type="submit" title="Save"> <strong> {vtranslate('LBL_SAVE', $MODULE)} </strong> </button>
				</div>
			</div>
		</div>
		<hr>
	</div>
	<div class="contents row-fluid">
		<table class="table table-bordered table-condensed listViewEntriesTable">
			<thead>
				<tr class="listViewHeaders">
					<th colspan="2" class="medium" width = "50%" nowrap>
						<span class="alignMiddle blockHeaderTitle"> <strong> {vtranslate('LBL_LIST_MODULES', $MODULE)} </strong> </span>
					</th>
					<th colspan="2" class="medium" width = "50%" nowrap>
						<span class="alignMiddle blockHeaderTitle"> <strong> {vtranslate('LBL_STATUS', $MODULE)} </strong> </span>
					</th>
					<th colspan="2" class="medium" width = "50%" nowrap>
                                                <span class="alignMiddle blockHeaderTitle"> <strong> {vtranslate('LBL_DOCUMENT_FOLDERS', $MODULE)} </strong> </span>
                                        </th>
				</tr>
			</thead>
			<tbody>
				{foreach from=$data key=data_key item=single_data}
				<tr>
					<td colspan="2" style="border-left: none;" class = 'listViewEntries medium' width = "33.33%">
						<label class="muted marginRight10px"> {vtranslate($single_data.moduleName)} </label>
					</td>
					<td colspan="2" style="border-left: none;" width = "33.33%" class = 'listViewEntries medium'> 
						<input type = 'checkbox' name = '{$single_data.moduleName}_status' id = '{$single_data.moduleName}_status' {$single_data.checked} class = 'iconic-check'>
                                                <label for="{$single_data.moduleName}_status" style = 'font-size: 1.5em; top: 5px; position: relative;'></label>
					</td>
					<td colspan="2" style="border-left: none;" width = "33.33%" class = 'listViewEntries medium'>
                                               <select name = 'documentfolders_{$single_data.moduleName}' id = 'documentfolders_{$single_data.moduleName}' class = 'chzn-select'>
                                                        {foreach from=$document_folders key=folder_id item=folder_name}
                                                                {assign var="df_selected" value=""}
                                                                {if $folder_id eq $single_data.folder_id}
                                                                	{assign var="df_selected" value="selected"}
                                                                {/if}
                                                        	<option value = '{$folder_id}' {$df_selected}> {$folder_name} </option>
                                                	{/foreach}
                                        	</select>
                                        </td>
				</tr>
				{/foreach}
			</tbody>
		</table>
	</div>
	<div class="contents row-fluid">
		<div class="widget_header row-fluid">
			<div class="span8">
				<h3> </h3>
			</div>
			<div class="span4">
				<div class="pull-right">
					<button class="btn editButton" type="submit" title="Save"> <strong> {vtranslate('LBL_SAVE', $MODULE)} </strong> </button>
				</div>
			</div>
		</div>
		<hr>
	</div>

</div>
</form>
<br> <br>
