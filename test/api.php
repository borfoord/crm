<?php
function getToken($url) {
	$url= 'http://crm.dmd.com/service/v2/rest.php';
	
	$curl = curl_init ($url);
	curl_setopt ( $curl, CURLOPT_POST, true );
	curl_setopt ( $curl, CURLOPT_HEADER, false );
	curl_setopt ( $curl, CURLOPT_RETURNTRANSFER, true );
	
	$parameters = array (
			'user_auth' => array (
					'user_name' => 'admin',
					'password' => md5 ( 'admin' ) 
			
			) 
	
	);
	
	$json = json_encode ( $parameters );
	$postArgs = array (
			'method' => 'login',
			'input_type' => 'JSON',
			'response_type' => 'JSON',
			'rest_data' => $json 
	);
	curl_setopt ( $curl, CURLOPT_POSTFIELDS, $postArgs );
	
	$response = curl_exec ( $curl );
	$result = json_decode ( $response );
	
	if (! is_object ( $result )) {
		die ( "Error handling result.\n" );
	}
	if (! isset ( $result->id )) {
		die ( "Error: {$result->name} - {$result->description}\n." );
	}
	
	return ($result->id);
}
/*
function CreateLead($first_name,$last_name,$email,$status,$lead_source) {
	$url= 'http://crm.dmd.com/service/v2/rest.php';
	$parameters = array (
			'session' => getToken(), // Session ID
			'module' => 'Leads', // Module name
			'name_value_list' => array (
					array (
							'name' => 'first_name',
							'value' => $first_name
					),
					array (
							'name' => 'last_name',
							'value' => $last_name
					),
					array (
							'name' => 'email1',
							'value' => $email
					),
					array (
							'name' => 'status',
							'value' => $status
					),
					array (
							'name' => 'lead_source',
							'value' => $lead_source
					),
					array (
							'name' => 'funnel_c',
							'value' => 'API'
					),
					array (
							'name' => 'leads_aos_product_categories_1_name',
							'value' => 'b9e86865-7794-bfe6-4130-59635c90978c'
					),
					array (
							'name' => 'language_code_c',
							'value' => 'AR'
					)	
			) 
	);
	$json = json_encode ( $parameters );
	$postArgs = 'method=set_entry&input_type=JSON&response_type=JSON&rest_data=' . $json;
	
	$curl = curl_init ($url);
	curl_setopt ( $curl, CURLOPT_POSTFIELDS, $postArgs );
	
	// Make the REST call, returning the result
	$response = curl_exec ( $curl );
	
	// Convert the result from JSON format to a PHP array
	$result = json_decode ( $response, true );
	
	// Get the newly created record id
	return($recordId = $result ['id']);
}

$time = time();
$return = CreateLead('test'.$time,'test'.$time,'test'.$time.'@gmail.com','New','New');

print_r($return);
echo "<hr>";
*/
function getLead($ID) {
	
	$fields_array = array('first_name','last_name','phone_work');
	
	$url= 'http://crm.dmd.com/service/v2/rest.php';
	$parameters = array (
			'session' => getToken(), // Session ID
			'module' => 'Leads', // Module name
			'name_value_list' => array (
					array(
						'name' => 'id',
						'value' => $ID,
							
					)
			)
	);
	$json = json_encode ( $parameters );
	$postArgs = 'method=set_entry&input_type=JSON&response_type=JSON&rest_data=' . $json;
	
	$curl = curl_init ($url);
	curl_setopt ( $curl, CURLOPT_POSTFIELDS, $postArgs );
	$response = curl_exec ( $curl );
	$result = json_decode ( $response, true );
	
	// Get the newly created record id
	print_r($result);
}

$return = getLead('1ce57fa4-8648-8721-a3e2-59623f98a1d4');

//print_r($return);
echo "<hr>";