/**
 * timezone.js
 */

'use strict';

var timezone = {};

// init data
fetch('tz.json')
.then(response => response.json())
.then(json => timezone.data=json);
//timezone.data = fetch('tz.json');

timezone.lookup = function(country, region) {
    return timezone.data[[country, region].join('_')] || timezone.data[[country, ''].join('_')];
};

//module.exports = timezone;