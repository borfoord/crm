<?php

define('TZFILE',dirname(__FILE__).'/tz.json');

class userClock{

	public static function userTime($country,$region){
		if(empty($country)){ return false;}
		$code = countryNames::name2iso($country);
		$search = $code.'_'.$region;
    $tzfile = json_decode(file_get_contents(TZFILE),true);
    if(empty($tz = $tzfile[$search])){
			$search = $code.'_';
			$tz = $tzfile[$search];
		}
		if(empty($tz)){ return false; }
		$date = new DateTime('now', new DateTimeZone($tz));
		$ret=array();
		$ret["ts"] = $date->getTimestamp();
		$ret["off"] = $date->getOffset()/3600;

		return $ret;
	}

} //end Class