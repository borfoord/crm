<?php

define('NAMES',dirname(__FILE__).'/names.json');
define('CODES',dirname(__FILE__).'/codes.json');

class countryNames {

        protected static $cCache = NULL;
        public static function name2iso($name){
                if(empty(self::$cCache[$name])){
                        $codes = json_decode(file_get_contents(CODES),true);
                        self::$cCache[$name] = $codes[$name];
                }
                return self::$cCache[$name];
        }

        public static function iso2name($iso){
                if(empty(self::$cCache[$iso])){
                        $names = json_decode(file_get_contents(NAMES),true);
                        self::$cCache[$iso] = $names[$iso];
                }
                return self::$cCache[$iso];
        }

}